/*
 Authors: Niels Aage, Erik Andreassen, Boyan Lazarov, August 2013

 Disclaimer:
 The authors reserves all rights but does not guaranty that the code is
 free from errors. Furthermore, we shall not be liable in any event
 caused by the use of the program.
*/

#include "linear_elasticity.h"

namespace para_fea {

LinearElasticity ::LinearElasticity (DM da_nodes, InitializeOpt &myInit) {
  // Set pointers to null
  K = NULL;
  U = NULL;
  RHS = NULL;
  N = NULL;
  ksp = NULL;
  da_nodal;

  // Parameters
  nu = myInit.nu;
  nlvls = myInit.nlvls;

  // Setup sitffness matrix, load vector and bcs (Dirichlet) for the design
  // problem
  SetUpLoadAndBC (da_nodes, myInit);
}


LinearElasticity ::~LinearElasticity () {
  // Deallocate
  VecDestroy (&(U));
  VecDestroy (&(RHS));
  VecDestroy (&(N));
  MatDestroy (&(K));
  KSPDestroy (&(ksp));
}


PetscErrorCode LinearElasticity ::SetUpLoadAndBC (DM da_nodes, InitializeOpt &myInit) {
  PetscErrorCode ierr;

  // Extract information from input DM and create one for the linear elasticity
  // number of nodal dofs: (u,v,w)
  PetscInt numnodaldof = 3;

  // Stencil width: each node connects to a box around it - linear elements
  PetscInt stencilwidth = 1;
  PetscScalar dx, dy, dz;
  DMBoundaryType bx, by, bz;
  DMDAStencilType stype;
  {
    // Extract information from the nodal mesh
    PetscInt M, N, P, md, nd, pd;
    DMDAGetInfo (da_nodes, NULL, &M, &N, &P, &md, &nd, &pd, NULL, NULL, &bx, &by, &bz, &stype);

    // Find the element size (dx, dy, dz)
    Vec lcoor;
    DMGetCoordinatesLocal (da_nodes, &lcoor);
    PetscScalar *lcoorp;
    VecGetArray (lcoor, &lcoorp);  // Get local coordinates in local node numbering
    PetscInt nel, nen;             // # elements and # nodes per element
    const PetscInt *necon;
    DMDAGetElements_3D (da_nodes, &nel, &nen, &necon);

    // Use the first element to compute the dx, dy, dz
    dx = lcoorp[3 * necon[0 * nen + 1] + 0] - lcoorp[3 * necon[0 * nen + 0] + 0];
    dy = lcoorp[3 * necon[0 * nen + 2] + 1] - lcoorp[3 * necon[0 * nen + 1] + 1];
    dz = lcoorp[3 * necon[0 * nen + 4] + 2] - lcoorp[3 * necon[0 * nen + 0] + 2];
    VecRestoreArray (lcoor, &lcoorp);

    nn[0] = M;  // number of nodes in the x
    nn[1] = N;  // number of nodes in the y
    nn[2] = P;  // number of nodes in the z

    ne[0] = nn[0] - 1;  // number of elements in the x
    ne[1] = nn[1] - 1;  // number of elements in the y
    ne[2] = nn[2] - 1;  // number of elements in the z

    // Domain coordinates (min and max values in each of the coordinate directions)
    xc[0] = 0.0;         // x min
    xc[1] = ne[0] * dx;  // x max
    xc[2] = 0.0;         // y min
    xc[3] = ne[1] * dy;  // y max
    xc[4] = 0.0;         // z min
    xc[5] = ne[2] * dz;  // z max
  }

  // Create the nodal mesh
  DMDACreate3d (PETSC_COMM_WORLD, bx, by, bz, stype, nn[0], nn[1], nn[2], PETSC_DECIDE,
                PETSC_DECIDE, PETSC_DECIDE, numnodaldof, stencilwidth, 0, 0, 0, &(da_nodal));
  // Initialize
  DMSetFromOptions (da_nodal);
  DMSetUp (da_nodal);

  // Set the coordinates
  DMDASetUniformCoordinates (da_nodal, xc[0], xc[1], xc[2], xc[3], xc[4], xc[5]);
  // Set the element type to Q1: Otherwise calls to GetElements will change to
  // P1 ! STILL DOESN*T WORK !!!!
  DMDASetElementType (da_nodal, DMDA_ELEMENT_Q1);

  // Allocate matrix and the RHS and Solution vector and Dirichlet vector
  ierr = DMCreateMatrix (da_nodal, &(K));
  CHKERRQ (ierr);
  ierr = DMCreateGlobalVector (da_nodal, &(U));
  CHKERRQ (ierr);
  VecDuplicate (U, &(RHS));
  VecDuplicate (U, &(N));

#ifndef SAVEMEMORY

  // for stress
  VecDuplicate (U, &(adU));
  VecDuplicate (U, &(adRHS));

  // for thermal force
  VecDuplicate (U, &(RHSthermal));

#endif

  // Set the local stiffness matrix
  PetscScalar X[8] = {0.0, dx, dx, 0.0, 0.0, dx, dx, 0.0};
  PetscScalar Y[8] = {0.0, 0.0, dy, dy, 0.0, 0.0, dy, dy};
  PetscScalar Z[8] = {0.0, 0.0, 0.0, 0.0, dz, dz, dz, dz};

  // Compute the element stiffnes matrix - constant due to structured grid
  Hex8Isoparametric (X, Y, Z, nu, false, KE);

  // Set the RHS and Dirichlet vector
  VecSet (N, 1.0);
  VecSet (RHS, 0.0);

#ifndef SAVEMEMORY

  VecSet (adRHS, 0.0);
  VecSet (adU, 0.0);

  // temporary
  VecDuplicate (U, &(RHS1));
  VecDuplicate (U, &(RHS2));
  VecSet (RHS1, 0.0);
  VecSet (RHS2, 0.0);

#endif

  // Global coordinates and a pointer
  Vec lcoor;  // borrowed ref - do not destroy!
  PetscScalar *lcoorp;

  // Get local coordinates in local node numbering including ghosts
  ierr = DMGetCoordinatesLocal (da_nodal, &lcoor);
  CHKERRQ (ierr);
  VecGetArray (lcoor, &lcoorp);

  // Get local dof number
  PetscInt nn;
  VecGetSize (lcoor, &nn);

  // Compute epsilon parameter for finding points in space:
  PetscScalar epsi = PetscMin (dx * 0.05, PetscMin (dy * 0.05, dz * 0.05));

  for (PetscInt i = 0; i < nn; i++) {
    if (i % 3 == 0) {
      double xNode = lcoorp[i];
      double yNode = lcoorp[i + 1];
      double zNode = lcoorp[i + 2];

      // boundary conditions
      for (int bc = 0; bc < myInit.fixedDofs.size (); bc++) {
        // see if at least one is fixed
        if (!myInit.fixedDofs[bc].valx || !myInit.fixedDofs[bc].valy
            || !myInit.fixedDofs[bc].valz) {
          // check for tolerance
          if (std::abs (xNode - myInit.fixedDofs[bc].x) <= myInit.fixedDofs[bc].tolx
              && std::abs (yNode - myInit.fixedDofs[bc].y) <= myInit.fixedDofs[bc].toly
              && std::abs (zNode - myInit.fixedDofs[bc].z) <= myInit.fixedDofs[bc].tolz) {
            // assign boundary conditions
            VecSetValueLocal (N, i, 1.0 * myInit.fixedDofs[bc].valx, INSERT_VALUES);
            VecSetValueLocal (N, i + 1, 1.0 * myInit.fixedDofs[bc].valy, INSERT_VALUES);
            VecSetValueLocal (N, i + 2, 1.0 * myInit.fixedDofs[bc].valz, INSERT_VALUES);
          }  // if tolerence
        }    // if fixed
      }      // bc loop

      // loading conditions
      // boundary conditions
      for (int load = 0; load < myInit.loads.size (); load++) {
        // check for tolerance
        if (std::abs (xNode - myInit.loads[load].x) <= myInit.loads[load].tolx
            && std::abs (yNode - myInit.loads[load].y) <= myInit.loads[load].toly
            && std::abs (zNode - myInit.loads[load].z) <= myInit.loads[load].tolz) {
          VecSetValueLocal (RHS, i, myInit.loads[load].valx, INSERT_VALUES);
          VecSetValueLocal (RHS, i + 1, myInit.loads[load].valy, INSERT_VALUES);
          VecSetValueLocal (RHS, i + 2, myInit.loads[load].valz, INSERT_VALUES);
#ifndef SAVEMEMORY
          // temporary
          VecSetValueLocal (RHS1, i, myInit.loads[load].valx, INSERT_VALUES);
          VecSetValueLocal (RHS2, i + 1, myInit.loads[load].valy, INSERT_VALUES);
#endif
        }  // if tolerence
      }    // load loop

#ifndef SAVEMEMORY
      for (int load = 0; load < myInit.adjLoads.size (); load++) {
        // check for tolerance
        if (std::abs (xNode - myInit.adjLoads[load].x) <= myInit.adjLoads[load].tolx
            && std::abs (yNode - myInit.adjLoads[load].y) <= myInit.adjLoads[load].toly
            && std::abs (zNode - myInit.adjLoads[load].z) <= myInit.adjLoads[load].tolz) {
          VecSetValueLocal (adRHS, i, myInit.adjLoads[load].valx, INSERT_VALUES);
          VecSetValueLocal (adRHS, i + 1, myInit.adjLoads[load].valy, INSERT_VALUES);
          VecSetValueLocal (adRHS, i + 2, myInit.adjLoads[load].valz, INSERT_VALUES);
        }  // if tolerence
      }    // load loop
#endif
    }  // if i%3 == 0
  }

  VecAssemblyBegin (N);
  VecAssemblyBegin (RHS);
  VecAssemblyEnd (N);
  VecAssemblyEnd (RHS);

#ifndef SAVEMEMORY
  // temporary
  VecAssemblyBegin (adRHS);
  VecAssemblyEnd (adRHS);
  VecAssemblyBegin (RHS1);
  VecAssemblyEnd (RHS1);
  VecAssemblyBegin (RHS2);
  VecAssemblyEnd (RHS2);
#endif

  VecRestoreArray (lcoor, &lcoorp);

  return ierr;
}


PetscErrorCode LinearElasticity ::SolveState (Vec xPhys, PetscScalar Emin, PetscScalar Emax,
                                              PetscScalar penal) {
  PetscErrorCode ierr;

  double t1 = MPI_Wtime ();

  // Assemble stiffnes matrix
  ierr = AssembleStiffnessMatrix (xPhys, Emin, Emax, penal);
  PetscPrintf (PETSC_COMM_WORLD, "AssembledStiffness \n");
  CHKERRQ (ierr);

  // Solve the state equation
  ierr = SolveStateHelper (t1);

  return ierr;
}


PetscErrorCode LinearElasticity ::SolveState (Vec xPhys, std::vector<Vec> momQuad, PetscScalar Emin,
                                              PetscScalar Emax, PetscScalar penal) {
  PetscErrorCode ierr;

  double t1 = MPI_Wtime ();

  // Assemble the stiffness matrix from custom quadrature
  ierr = AssembleStiffnessMatrixFromQuadrature (xPhys, momQuad, Emin, Emax, penal);
  PetscPrintf (PETSC_COMM_WORLD, "AssembledStiffness \n");
  CHKERRQ (ierr);

  // Solve the state equation
  ierr = SolveStateHelper (t1);

  return ierr;
}


PetscErrorCode LinearElasticity ::SolveState (Vec xPhys, Vec delta_t, PetscScalar Emin,
                                              PetscScalar Emax, PetscScalar alpha_t,
                                              PetscScalar penal) {
  PetscErrorCode ierr;

  double t1 = MPI_Wtime ();

  // Assemble stiffness matrix
  ierr = AssembleStiffnessMatrix (xPhys, Emin, Emax, penal);
  PetscPrintf (PETSC_COMM_WORLD, "AssembledStiffness \n");
  CHKERRQ (ierr);

  // Assemble the thermal expansion loading vector
  ierr = AssembleThermalExpansionLoad (xPhys, delta_t, Emin, Emax, alpha_t, penal);
  PetscPrintf (PETSC_COMM_WORLD, "AssembledThermalLoading\n");
  CHKERRQ (ierr);

  // Add thermal load to RHS
  VecAXPY (RHS, 1.0, RHSthermal);

  // Solve the state equation
  ierr = SolveStateHelper (t1);

  return ierr;
}


PetscErrorCode LinearElasticity ::SolveState (Vec xPhys, std::vector<Vec> momQuad, Vec delta_t,
                                              PetscScalar Emin, PetscScalar Emax,
                                              PetscScalar alpha_t, PetscScalar penal) {
  PetscErrorCode ierr;

  double t1 = MPI_Wtime ();

  // Assemble stiffness matrix from quadrature
  ierr = AssembleStiffnessMatrixFromQuadrature (xPhys, momQuad, Emin, Emax, penal);
  PetscPrintf (PETSC_COMM_WORLD, "AssembledStiffness \n");
  CHKERRQ (ierr);

  // Assemble the thermal expansion loading vector
  ierr = AssembleThermalExpansionLoad (xPhys, delta_t, Emin, Emax, alpha_t, penal);
  PetscPrintf (PETSC_COMM_WORLD, "AssembledThermalLoading\n");
  CHKERRQ (ierr);

  // Add thermal load to RHS
  VecAXPY (RHS, 1.0, RHSthermal);

  // Solve the state equation
  ierr = SolveStateHelper (t1);

  return ierr;
}


PetscErrorCode LinearElasticity ::SolveStateHelper (double t1) {
  PetscErrorCode ierr;

  // Setup the solver
  if (ksp == NULL) {
    ierr = SetUpSolver ();
    CHKERRQ (ierr);
  } else {
    ierr = KSPSetOperators (ksp, K, K);
    CHKERRQ (ierr);
    KSPSetUp (ksp);
  }

  // Solve
  ierr = KSPSolve (ksp, RHS, U);
  CHKERRQ (ierr);

  // DEBUG
  // Get iteration number and residual from KSP
  PetscInt niter;
  PetscScalar rnorm;
  KSPGetIterationNumber (ksp, &niter);
  KSPGetResidualNorm (ksp, &rnorm);
  PetscReal RHSnorm;
  ierr = VecNorm (RHS, NORM_2, &RHSnorm);
  CHKERRQ (ierr);
  rnorm = rnorm / RHSnorm;

  double t2 = MPI_Wtime ();
  PetscPrintf (PETSC_COMM_WORLD, "State solver:  iter: %i, rerr.: %e, time: %f\n", niter, rnorm,
               t2 - t1);

  return ierr;
}


PetscErrorCode LinearElasticity ::SolveAdjointState (Vec xPhys, PetscScalar Emin, PetscScalar Emax,
                                                     PetscScalar penal) {
  PetscErrorCode ierr;

  double t1, t2;
  t1 = MPI_Wtime ();

  // Assemble the stiffness matrix
  ierr = AssembleStiffnessMatrix (xPhys, Emin, Emax, penal);
  PetscPrintf (PETSC_COMM_WORLD, "AssembledStiffness \n");
  CHKERRQ (ierr);

  // Setup the solver
  if (ksp == NULL) {
    ierr = SetUpSolver ();
    CHKERRQ (ierr);
  } else {
    ierr = KSPSetOperators (ksp, K, K);
    CHKERRQ (ierr);
    KSPSetUp (ksp);
  }

  // Solve
  VecSet (adU, 0.0);
  ierr = KSPSolve (ksp, adRHS, adU);
  CHKERRQ (ierr);

  // DEBUG
  // Get iteration number and residual from KSP
  PetscInt niter;
  PetscScalar rnorm;
  KSPGetIterationNumber (ksp, &niter);
  KSPGetResidualNorm (ksp, &rnorm);
  PetscReal RHSnorm;
  ierr = VecNorm (adRHS, NORM_2, &RHSnorm);
  CHKERRQ (ierr);
  rnorm = rnorm / RHSnorm;

  t2 = MPI_Wtime ();
  PetscPrintf (PETSC_COMM_WORLD, "Adjoint State solver:  iter: %i, rerr.: %e, time: %f\n", niter,
               rnorm, t2 - t1);

  return ierr;
}


PetscErrorCode LinearElasticity ::SolveThermalOnlyState (Vec xPhys, Vec delta_t, PetscScalar Emin,
                                                         PetscScalar Emax, PetscScalar alpha_t,
                                                         PetscScalar penal) {
  PetscErrorCode ierr;

  double t1, t2;
  t1 = MPI_Wtime ();

  // Assemble the stiffness matrix
  ierr = AssembleStiffnessMatrix (xPhys, Emin, Emax, penal);
  PetscPrintf (PETSC_COMM_WORLD, "AssembledStiffness \n");
  CHKERRQ (ierr);

  // Assemble the thermal expansion loading vector
  ierr = AssembleThermalExpansionLoad (xPhys, delta_t, Emin, Emax, alpha_t, penal);
  PetscPrintf (PETSC_COMM_WORLD, "AssembledThermalLoading\n");
  CHKERRQ (ierr);

  // Setup the solver
  if (ksp == NULL) {
    ierr = SetUpSolver ();
    CHKERRQ (ierr);
  } else {
    ierr = KSPSetOperators (ksp, K, K);
    CHKERRQ (ierr);
    KSPSetUp (ksp);
  }

  // Solve
  ierr = KSPSolve (ksp, RHSthermal, U);
  CHKERRQ (ierr);

  // DEBUG
  // Get iteration number and residual from KSP
  PetscInt niter;
  PetscScalar rnorm;
  KSPGetIterationNumber (ksp, &niter);
  KSPGetResidualNorm (ksp, &rnorm);
  PetscReal RHSnorm;
  ierr = VecNorm (RHSthermal, NORM_2, &RHSnorm);
  CHKERRQ (ierr);
  rnorm = rnorm / RHSnorm;

  t2 = MPI_Wtime ();
  PetscPrintf (PETSC_COMM_WORLD, "Thermal State solver:  iter: %i, rerr.: %e, time: %f\n", niter,
               rnorm, t2 - t1);

  return ierr;
}


PetscErrorCode LinearElasticity ::ComputeSensitivities (PetscScalar *fx, Vec dfdx, Vec xPhys,
                                                        PetscScalar Emin, PetscScalar Emax,
                                                        PetscScalar penal) {
  // Errorcode
  PetscErrorCode ierr;

  // Solve state eqs
  ierr = SolveState (xPhys, Emin, Emax, penal);
  CHKERRQ (ierr);

  // Get the FE mesh structure (from the nodal mesh)
  PetscInt nel, nen;
  const PetscInt *necon;
  ierr = DMDAGetElements_3D (da_nodal, &nel, &nen, &necon);
  CHKERRQ (ierr);
  // DMDAGetElements(da_nodes,&nel,&nen,&necon); // Still issue with elemtype
  // change !

  // Get pointer to the densities
  PetscScalar *xp;
  VecGetArray (xPhys, &xp);

  // Get Solution
  Vec Uloc;
  DMCreateLocalVector (da_nodal, &Uloc);
  DMGlobalToLocalBegin (da_nodal, U, INSERT_VALUES, Uloc);
  DMGlobalToLocalEnd (da_nodal, U, INSERT_VALUES, Uloc);

  // get pointer to local vector
  PetscScalar *up;
  VecGetArray (Uloc, &up);

  // Get dfdx
  PetscScalar *df;
  VecGetArray (dfdx, &df);

  // Edof array
  PetscInt edof[24];

  fx[0] = 0.0;
  // Loop over elements
  for (PetscInt i = 0; i < nel; i++) {
    // loop over element nodes
    for (PetscInt j = 0; j < nen; j++) {
      // Get local dofs
      for (PetscInt k = 0; k < 3; k++) {
        edof[j * 3 + k] = 3 * necon[i * nen + j] + k;
      }
    }
    // Use SIMP for stiffness interpolation
    PetscScalar uKu = 0.0;
    for (PetscInt k = 0; k < 24; k++) {
      for (PetscInt h = 0; h < 24; h++) {
        uKu += up[edof[k]] * KE[k * 24 + h] * up[edof[h]];
      }
    }
    // Add to objective
    fx[0] += (Emin + PetscPowScalar (xp[i], penal) * (Emax - Emin)) * uKu;
    // Set the Senstivity
    df[i] = -1.0 * penal * PetscPowScalar (xp[i], penal - 1) * (Emax - Emin) * uKu;
  }

  // Allreduce fx[0]
  PetscScalar tmp = fx[0];
  fx[0] = 0.0;
  MPI_Allreduce (&tmp, &(fx[0]), 1, MPIU_SCALAR, MPI_SUM, PETSC_COMM_WORLD);

  // Compute compliance objective
  PetscScalar dotp1;
  VecDot (U, RHS, &dotp1);
  fx[0] = dotp1;

  VecRestoreArray (xPhys, &xp);
  VecRestoreArray (Uloc, &up);
  VecRestoreArray (dfdx, &df);
  VecDestroy (&Uloc);

  return (ierr);
}


// TODO: delete method after debugging
PetscErrorCode LinearElasticity ::ComputeSensitivitiesTest (PetscScalar *fx, Vec dfdx, Vec xPhys,
                                                            PetscScalar Emin, PetscScalar Emax,
                                                            PetscScalar penal) {
  // Errorcode
  PetscErrorCode ierr;

  // Get the FE mesh structure (from the nodal mesh)
  PetscInt nel, nen;
  const PetscInt *necon;
  ierr = DMDAGetElements_3D (da_nodal, &nel, &nen, &necon);
  CHKERRQ (ierr);

  // Get pointer to the densities
  PetscScalar *xp;
  VecGetArray (xPhys, &xp);

  // Get Solution
  Vec Uloc;
  DMCreateLocalVector (da_nodal, &Uloc);
  DMGlobalToLocalBegin (da_nodal, U, INSERT_VALUES, Uloc);
  DMGlobalToLocalEnd (da_nodal, U, INSERT_VALUES, Uloc);

  // get pointer to local vector
  PetscScalar *up;
  VecGetArray (Uloc, &up);

  // Get dfdx
  PetscScalar *df;
  VecGetArray (dfdx, &df);

  // Edof array
  PetscInt edof[24];

  fx[0] = 0.0;
  // Loop over elements
  for (PetscInt i = 0; i < nel; i++) {
    // loop over element nodes
    for (PetscInt j = 0; j < nen; j++) {
      // Get local dofs
      for (PetscInt k = 0; k < 3; k++) {
        edof[j * 3 + k] = 3 * necon[i * nen + j] + k;
      }
    }
    // Use SIMP for stiffness interpolation
    PetscScalar uKu = 0.0;
    for (PetscInt k = 0; k < 24; k++) {
      for (PetscInt h = 0; h < 24; h++) {
        uKu += up[edof[k]] * KE[k * 24 + h] * up[edof[h]];
      }
    }
    // Add to objective
    fx[0] += (Emin + PetscPowScalar (xp[i], penal) * (Emax - Emin)) * uKu;
    // Set the Senstivity
    df[i] = -1.0 * penal * PetscPowScalar (xp[i], penal - 1) * (Emax - Emin) * uKu;
  }

  // Allreduce fx[0]
  PetscScalar tmp = fx[0];
  fx[0] = 0.0;
  MPI_Allreduce (&tmp, &(fx[0]), 1, MPIU_SCALAR, MPI_SUM, PETSC_COMM_WORLD);

  // Compute compliance objective
  PetscScalar dotp1;
  VecDot (U, RHS, &dotp1);
  fx[0] = dotp1;

  VecRestoreArray (xPhys, &xp);
  VecRestoreArray (Uloc, &up);
  VecRestoreArray (dfdx, &df);
  VecDestroy (&Uloc);

  return (ierr);
}


PetscErrorCode LinearElasticity ::ComputeSensitivitiesFromQuadrature (
    PetscScalar *fx, Vec dfdx, Vec xPhys, std::vector<Vec> momQuad, PetscScalar Emin,
    PetscScalar Emax, PetscScalar penal) {
  // Errorcode
  PetscErrorCode ierr;

  // Solve state eqs
  ierr = SolveState (xPhys, momQuad, Emin, Emax, penal);
  CHKERRQ (ierr);

  // Get the FE mesh structure (from the nodal mesh)
  PetscInt nel, nen;
  const PetscInt *necon;
  ierr = DMDAGetElements_3D (da_nodal, &nel, &nen, &necon);
  CHKERRQ (ierr);

  // Get pointer to the densities
  PetscScalar *xp;
  VecGetArray (xPhys, &xp);

  // Get pointer to quadrature
  std::vector<PetscScalar *> momQuadPointer (27);
  for (int i = 0; i < 27; i++) {
    VecGetArray (momQuad[i], &momQuadPointer[i]);
  }

  // Get Solution
  Vec Uloc;
  DMCreateLocalVector (da_nodal, &Uloc);
  DMGlobalToLocalBegin (da_nodal, U, INSERT_VALUES, Uloc);
  DMGlobalToLocalEnd (da_nodal, U, INSERT_VALUES, Uloc);

  // get pointer to local vector
  PetscScalar *up;
  VecGetArray (Uloc, &up);

  // Get dfdx
  PetscScalar *df;
  VecGetArray (dfdx, &df);

  // Edof array
  PetscInt edof[24];
  PetscScalar ke[24 * 24];  // stiffness matrix elemental

  // Set the local stiffness matrix
  PetscScalar dx = (xc[1] - xc[0]) / (PetscScalar (ne[0]));  // x-side length
  PetscScalar dy = (xc[3] - xc[2]) / (PetscScalar (ne[1]));  // y-side length
  PetscScalar dz = (xc[5] - xc[4]) / (PetscScalar (ne[2]));  // z-side length
  PetscScalar X[8] = {0.0, dx, dx, 0.0, 0.0, dx, dx, 0.0};
  PetscScalar Y[8] = {0.0, 0.0, dy, dy, 0.0, 0.0, dy, dy};
  PetscScalar Z[8] = {0.0, 0.0, 0.0, 0.0, dz, dz, dz, dz};

  fx[0] = 0.0;
  // Loop over elements
  for (PetscInt i = 0; i < nel; i++) {
    // loop over element nodes
    for (PetscInt j = 0; j < nen; j++) {
      // Get local dofs
      for (PetscInt k = 0; k < 3; k++) {
        edof[j * 3 + k] = 3 * necon[i * nen + j] + k;
      }
    }

    PetscScalar dens = Emin + PetscPowScalar (xp[i], penal) * (Emax - Emin);

    if (xp[i] == 1.0 || xp[i] == 0.0) {
      for (PetscInt k = 0; k < 24 * 24; k++) {
        ke[k] = KE[k] * dens;
      }
    } else {
      std::vector<double> elemQuadrature (27, 0.0);
      double quadSum = 0.0;
      for (int quadNumber = 0; quadNumber < 27; quadNumber++) {
        elemQuadrature[quadNumber] = momQuadPointer[quadNumber][i];
        quadSum += elemQuadrature[quadNumber] / 8.0;
      }
      std::vector<double> keMom = Hex8Isoparametric (X, Y, Z, 0.3, false, elemQuadrature);

      for (PetscInt k = 0; k < 24 * 24; k++) {
        // ke[k]=KE[k]*dens;
        ke[k] = KE[k] * Emin + Emax * (keMom[k]) / (quadSum);
      }
    }

    // compute strain energy stored in an element
    PetscScalar uKu = 0.0;
    for (PetscInt k = 0; k < 24; k++) {
      for (PetscInt h = 0; h < 24; h++) {
        uKu += up[edof[k]] * ke[k * 24 + h] * up[edof[h]];
        // uKu += up[edof[k]]*KE[k*24+h]*up[edof[h]]*Emax;
      }
    }
    // Set the Senstivity
    df[i] = -1.0 * uKu;
  }

  // Allreduce fx[0]
  PetscScalar tmp = fx[0];
  fx[0] = 0.0;
  MPI_Allreduce (&tmp, &(fx[0]), 1, MPIU_SCALAR, MPI_SUM, PETSC_COMM_WORLD);

  PetscScalar dotp1;
  VecDot (U, RHS, &dotp1);

  fx[0] = dotp1;

  VecRestoreArray (xPhys, &xp);

  for (int i = 0; i < 27; i++) {
    VecRestoreArray (momQuad[i], &momQuadPointer[i]);
  }

  VecRestoreArray (Uloc, &up);
  VecRestoreArray (dfdx, &df);
  VecDestroy (&Uloc);

  return (ierr);
}


PetscErrorCode LinearElasticity ::ComputeElemDirDisp (Vec elemDisp, Vec xPhys,
                                                      std::vector<double> dirArr) {
  // Errorcode
  PetscErrorCode ierr;

  // Get the FE mesh structure (from the nodal mesh)
  PetscInt nel, nen;
  const PetscInt *necon;
  ierr = DMDAGetElements_3D (da_nodal, &nel, &nen, &necon);
  CHKERRQ (ierr);

  // Get pointer to the densities
  PetscScalar *xp;
  VecGetArray (xPhys, &xp);

  // Get Solution
  Vec Uloc;
  DMCreateLocalVector (da_nodal, &Uloc);
  DMGlobalToLocalBegin (da_nodal, U, INSERT_VALUES, Uloc);
  DMGlobalToLocalEnd (da_nodal, U, INSERT_VALUES, Uloc);

  // get pointer to local vector
  PetscScalar *up;
  VecGetArray (Uloc, &up);

  // Get element displacement
  PetscScalar *df;
  VecGetArray (elemDisp, &df);

  // Edof array
  PetscInt edof[24];

  // Loop over elements
  for (PetscInt i = 0; i < nel; i++) {
    // loop over element nodes
    for (PetscInt j = 0; j < nen; j++) {
      // Get local dofs
      for (PetscInt k = 0; k < 3; k++) {
        edof[j * 3 + k] = 3 * necon[i * nen + j] + k;
      }
    }
    // Calculate displacement dot product
    PetscScalar dotp = 0.0;
    for (PetscInt k = 0; k < 24; k++) {
      dotp += 0.125 * up[edof[k]] * dirArr[(k % 3)];
    }
    df[i] = dotp;
  }

  VecRestoreArray (xPhys, &xp);
  VecRestoreArray (Uloc, &up);
  VecRestoreArray (elemDisp, &df);
  VecDestroy (&Uloc);

  return (ierr);
}


PetscErrorCode LinearElasticity ::ComputeElemMagDisp (Vec elemDisp, Vec xPhys) {
  // Errorcode
  PetscErrorCode ierr;

  // Get the FE mesh structure (from the nodal mesh)
  PetscInt nel, nen;
  const PetscInt *necon;
  ierr = DMDAGetElements_3D (da_nodal, &nel, &nen, &necon);
  CHKERRQ (ierr);

  // Get pointer to the densities
  PetscScalar *xp;
  VecGetArray (xPhys, &xp);

  // Get Solution
  Vec Uloc;
  DMCreateLocalVector (da_nodal, &Uloc);
  DMGlobalToLocalBegin (da_nodal, U, INSERT_VALUES, Uloc);
  DMGlobalToLocalEnd (da_nodal, U, INSERT_VALUES, Uloc);

  // get pointer to local vector
  PetscScalar *up;
  VecGetArray (Uloc, &up);

  // Get dfdx
  PetscScalar *df;
  VecGetArray (elemDisp, &df);

  // Edof array
  PetscInt edof[24];

  // Loop over elements
  for (PetscInt i = 0; i < nel; i++) {
    // loop over element nodes
    for (PetscInt j = 0; j < nen; j++) {
      // Get local dofs
      for (PetscInt k = 0; k < 3; k++) {
        edof[j * 3 + k] = 3 * necon[i * nen + j] + k;
      }
    }
    // Calculate element displacement magnitude
    PetscScalar ux = 0.0;
    PetscScalar uy = 0.0;
    PetscScalar uz = 0.0;
    for (PetscInt k = 0; k < 24; k++) {
      if (k % 3 == 0) ux += 0.125 * up[edof[k]];
      if (k % 3 == 1) uy += 0.125 * up[edof[k]];
      if (k % 3 == 2) uz += 0.125 * up[edof[k]];
    }
    df[i] = std::sqrt (ux * ux + uy * uy + uz * uz);
  }

  VecRestoreArray (xPhys, &xp);
  VecRestoreArray (Uloc, &up);
  VecRestoreArray (elemDisp, &df);
  VecDestroy (&Uloc);

  return (ierr);
}


PetscErrorCode LinearElasticity ::ComputeThRotSensitivities (PetscScalar *fx, Vec dfdx, Vec xPhys,
                                                             Vec DeltaTVec, Vec DeltaTVecAdj,
                                                             PetscScalar Emin, PetscScalar Emax,
                                                             PetscScalar alpha_t,
                                                             PetscScalar penal) {
  // Errorcode
  PetscErrorCode ierr;

  // Solve state eqs
  ierr = SolveThermalOnlyState (xPhys, DeltaTVec, Emin, Emax, alpha_t, penal);
  CHKERRQ (ierr);

  // Solve adjooint state eqs
  ierr = SolveAdjointState (xPhys, Emin, Emax, penal);
  CHKERRQ (ierr);

  // Get the FE mesh structure (from the nodal mesh)
  PetscInt nel, nen;
  const PetscInt *necon;
  ierr = DMDAGetElements_3D (da_nodal, &nel, &nen, &necon);
  CHKERRQ (ierr);

  // Get pointer to the densities
  PetscScalar *xp;
  VecGetArray (xPhys, &xp);

  // Get pointer to the element temperature
  PetscScalar *DeltaTVecp;
  VecGetArray (DeltaTVec, &DeltaTVecp);

  // Get pointer to the element adjoint temperature
  PetscScalar *DeltaTVecAdjp;
  VecGetArray (DeltaTVecAdj, &DeltaTVecAdjp);

  // Get Solution
  Vec Uloc;
  DMCreateLocalVector (da_nodal, &Uloc);
  DMGlobalToLocalBegin (da_nodal, U, INSERT_VALUES, Uloc);
  DMGlobalToLocalEnd (da_nodal, U, INSERT_VALUES, Uloc);

  // Get pointer to adjoint solution
  Vec LAMloc;
  DMCreateLocalVector (da_nodal, &LAMloc);
  DMGlobalToLocalBegin (da_nodal, adU, INSERT_VALUES, LAMloc);
  DMGlobalToLocalEnd (da_nodal, adU, INSERT_VALUES, LAMloc);

  // get pointer to local vector
  PetscScalar *up;
  VecGetArray (Uloc, &up);

  PetscScalar *lamp;
  VecGetArray (LAMloc, &lamp);

  // Get dfdx
  PetscScalar *df;
  VecGetArray (dfdx, &df);

  // Edof array
  PetscInt edof[24];

  fx[0] = 0.0;
  // Loop over elements
  for (PetscInt i = 0; i < nel; i++) {
    // loop over element nodes
    for (PetscInt j = 0; j < nen; j++) {
      // Get local dofs
      for (PetscInt k = 0; k < 3; k++) {
        edof[j * 3 + k] = 3 * necon[i * nen + j] + k;
      }
    }
    // Use SIMP for stiffness interpolation
    PetscScalar lamKu = 0.0;
    for (PetscInt k = 0; k < 24; k++) {
      for (PetscInt h = 0; h < 24; h++) {
        lamKu += lamp[edof[k]] * KE[k * 24 + h] * up[edof[h]];
        // uKu += up[edof[k]]*KE_local[k*24+h]*up[edof[h]];
      }
    }
    // calculate fthermal for unit densitiy
    PetscScalar lamFt = 0.0;
    PetscScalar lamFth = 0.0;
    for (PetscInt k = 0; k < 24; k++) {
      // this the derivative of fe_thermal at an element w.r.t densitiy of that element
      PetscScalar fe_thermal_k = fThhermalElem[k] * alpha_t * DeltaTVecp[i];
      lamFt += lamp[edof[k]] * fe_thermal_k;
      lamFth += lamp[edof[k]] * fThhermalElem[k] * alpha_t * (Emin + xp[i] * (Emax - Emin));
    }

    // Set the Senstivity
    df[i] = -1.0 * penal * PetscPowScalar (xp[i], penal - 1) * (Emax - Emin) * lamKu;
    df[i] += 1.0 * penal * PetscPowScalar (xp[i], penal - 1) * (Emax - Emin) * lamFt;

    DeltaTVecAdjp[i] = lamFth;
  }

  VecRestoreArray (xPhys, &xp);
  VecRestoreArray (Uloc, &up);
  VecRestoreArray (dfdx, &df);
  VecDestroy (&Uloc);
  VecRestoreArray (LAMloc, &lamp);
  VecDestroy (&LAMloc);

  VecRestoreArray (DeltaTVec, &DeltaTVecp);
  VecRestoreArray (DeltaTVecAdj, &DeltaTVecAdjp);

  PetscScalar dotp1;
  VecDot (U, adRHS, &dotp1);
  fx[0] = dotp1;

  return (ierr);
}


//##################################################################
//##################################################################
//##################################################################
// ######################## PRIVATE ################################
//##################################################################
//##################################################################

PetscErrorCode LinearElasticity ::AssembleStiffnessMatrix (Vec xPhys, PetscScalar Emin,
                                                           PetscScalar Emax, PetscScalar penal) {
  PetscErrorCode ierr;

  // Get the FE mesh structure (from the nodal mesh)
  PetscInt nel, nen;
  const PetscInt *necon;
  ierr = DMDAGetElements_3D (da_nodal, &nel, &nen, &necon);
  CHKERRQ (ierr);

  // Get pointer to the densities
  PetscScalar *xp;
  VecGetArray (xPhys, &xp);

  // Zero the matrix
  MatZeroEntries (K);

  // Edof array
  PetscInt edof[24];
  PetscScalar ke[24 * 24];

  // Compute stiffness matrix for each element
  for (PetscInt i = 0; i < nel; i++) {    // loop over elements
    for (PetscInt j = 0; j < nen; j++) {  // loop over element nodes
      // Get local dofs
      for (PetscInt k = 0; k < 3; k++) {
        edof[j * 3 + k] = 3 * necon[i * nen + j] + k;
      }
    }
    // Use SIMP for stiffness interpolation
    PetscScalar dens = Emin + PetscPowScalar (xp[i], penal) * (Emax - Emin);
    for (PetscInt k = 0; k < 24 * 24; k++) {
      ke[k] = KE[k] * dens;
    }

    // Add values to the sparse matrix
    ierr = MatSetValuesLocal (K, 24, edof, 24, edof, ke, ADD_VALUES);
    CHKERRQ (ierr);
  }
  MatAssemblyBegin (K, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd (K, MAT_FINAL_ASSEMBLY);

  // Impose the dirichlet conditions, i.e. K = N'*K*N - (N-I)
  // 1.: K = N'*K*N
  MatDiagonalScale (K, N, N);
  // 2. Add ones, i.e. K = K + NI, NI = I - N
  Vec NI;
  VecDuplicate (N, &NI);
  VecSet (NI, 1.0);
  VecAXPY (NI, -1.0, N);
  MatDiagonalSet (K, NI, ADD_VALUES);

  // Zero out possible loads in the RHS that coincide
  // with Dirichlet conditions
  VecPointwiseMult (RHS, RHS, N);

  VecDestroy (&NI);
  VecRestoreArray (xPhys, &xp);

  DMDARestoreElements (da_nodal, &nel, &nen, &necon);

  return ierr;
}


PetscErrorCode LinearElasticity ::AssembleStiffnessMatrixFromQuadrature (
    Vec xPhys, std::vector<Vec> momQuad, PetscScalar Emin, PetscScalar Emax, PetscScalar penal) {
  PetscErrorCode ierr;

  // Get the FE mesh structure (from the nodal mesh)
  PetscInt nel, nen;
  const PetscInt *necon;
  ierr = DMDAGetElements_3D (da_nodal, &nel, &nen, &necon);
  CHKERRQ (ierr);

  // Get pointer to the densities
  PetscScalar *xp;
  VecGetArray (xPhys, &xp);

  // Get pointer to quadrature
  std::vector<PetscScalar *> momQuadPointer (27);
  for (int i = 0; i < 27; i++) {
    VecGetArray (momQuad[i], &momQuadPointer[i]);
  }

  // Zero the matrix
  MatZeroEntries (K);

  // Edof array
  PetscInt edof[24];
  PetscScalar ke[24 * 24];

  // Set the local stiffness matrix
  PetscScalar dx = (xc[1] - xc[0]) / (PetscScalar (ne[0]));  // x-side length
  PetscScalar dy = (xc[3] - xc[2]) / (PetscScalar (ne[1]));  // y-side length
  PetscScalar dz = (xc[5] - xc[4]) / (PetscScalar (ne[2]));  // z-side length
  PetscScalar X[8] = {0.0, dx, dx, 0.0, 0.0, dx, dx, 0.0};
  PetscScalar Y[8] = {0.0, 0.0, dy, dy, 0.0, 0.0, dy, dy};
  PetscScalar Z[8] = {0.0, 0.0, 0.0, 0.0, dz, dz, dz, dz};

  // Loop over elements
  for (PetscInt i = 0; i < nel; i++) {
    // loop over element nodes
    for (PetscInt j = 0; j < nen; j++) {
      // Get local dofs
      for (PetscInt k = 0; k < 3; k++) {
        edof[j * 3 + k] = 3 * necon[i * nen + j] + k;
      }
    }
    // Use SIMP for stiffness interpolation
    PetscScalar dens = Emin + PetscPowScalar (xp[i], penal) * (Emax - Emin);

    if (xp[i] == 1.0 || xp[i] == 0.0) {
      for (PetscInt k = 0; k < 24 * 24; k++) {
        ke[k] = KE[k] * dens;
      }
    } else {
      std::vector<double> elemQuadrature (27, 0.0);
      for (int quadNumber = 0; quadNumber < 27; quadNumber++) {
        elemQuadrature[quadNumber] = momQuadPointer[quadNumber][i];
      }
      std::vector<double> keMom = Hex8Isoparametric (X, Y, Z, 0.3, false, elemQuadrature);

      for (PetscInt k = 0; k < 24 * 24; k++) {
        // ke[k]=KE[k]*dens;
        ke[k] = KE[k] * Emin + Emax * (keMom[k]);
      }
    }

    // Add values to the sparse matrix
    ierr = MatSetValuesLocal (K, 24, edof, 24, edof, ke, ADD_VALUES);
    CHKERRQ (ierr);
  }
  MatAssemblyBegin (K, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd (K, MAT_FINAL_ASSEMBLY);

  // Impose the dirichlet conditions, i.e. K = N'*K*N - (N-I)
  // 1.: K = N'*K*N
  MatDiagonalScale (K, N, N);
  // 2. Add ones, i.e. K = K + NI, NI = I - N
  Vec NI;
  VecDuplicate (N, &NI);
  VecSet (NI, 1.0);
  VecAXPY (NI, -1.0, N);
  MatDiagonalSet (K, NI, ADD_VALUES);

  // Zero out possible loads in the RHS that coincide
  // with Dirichlet conditions
  VecPointwiseMult (RHS, RHS, N);

  VecDestroy (&NI);
  VecRestoreArray (xPhys, &xp);

  for (int i = 0; i < 27; i++) {
    VecRestoreArray (momQuad[i], &momQuadPointer[i]);
  }

  return ierr;
}


PetscErrorCode LinearElasticity ::AssembleThermalExpansionLoad (Vec xPhys, Vec delta_t,
                                                                PetscScalar Emin, PetscScalar Emax,
                                                                PetscScalar alphaT,
                                                                PetscScalar penal) {
  PetscErrorCode ierr;

  // Get the FE mesh structure (from the nodal mesh)
  PetscInt nel, nen;
  const PetscInt *necon;
  ierr = DMDAGetElements_3D (da_nodal, &nel, &nen, &necon);
  CHKERRQ (ierr);

  // Get pointer to the densities
  PetscScalar *xp;
  VecGetArray (xPhys, &xp);

  // Get pointer to the element temperature
  PetscScalar *DeltaTVecp;
  VecGetArray (delta_t, &DeltaTVecp);

  // Edof array
  PetscInt edof[24];
  PetscScalar fe_thermal[24];

  VecSet (RHSthermal, 0.0);

  // Compute thermal expansion load vector for each element
  for (PetscInt i = 0; i < nel; i++) {    // loop over elements
    for (PetscInt j = 0; j < nen; j++) {  // loop over element nodes
      // Get local dofs
      for (PetscInt k = 0; k < 3; k++) {
        edof[j * 3 + k] = 3 * necon[i * nen + j] + k;
      }
    }
    // Use SIMP for stiffness interpolation
    PetscScalar dens = Emin + PetscPowScalar (xp[i], penal) * (Emax - Emin);

    // multiply vol frac, alpha*DeltaT to thermal load
    for (PetscInt k = 0; k < 24; k++) {
      fe_thermal[k] = fThhermalElem[k] * dens * alphaT * DeltaTVecp[i];
    }

    ierr = VecSetValuesLocal (RHSthermal, 24, edof, fe_thermal, ADD_VALUES);
    CHKERRQ (ierr);
  }
  VecAssemblyBegin (RHSthermal);
  VecAssemblyEnd (RHSthermal);

  // Zero out possible loads in the RHSthermal that coincide with Dirichlet
  // conditions
  VecPointwiseMult (RHSthermal, RHSthermal, N);

  VecRestoreArray (xPhys, &xp);
  VecRestoreArray (delta_t, &DeltaTVecp);
  DMDARestoreElements (da_nodal, &nel, &nen, &necon);

  return ierr;
}


PetscErrorCode LinearElasticity ::SetUpSolver () {
  PetscErrorCode ierr;
  PC pc;

  // The fine grid Krylov method
  KSPCreate (PETSC_COMM_WORLD, &(ksp));

  // SET THE DEFAULT SOLVER PARAMETERS
  // The fine grid solver settings
  PetscScalar rtol = myRTOL;
  PetscScalar atol = 1.0e-50;
  PetscScalar dtol = 1.0e3;
  PetscInt restart = 200;
  PetscInt maxitsGlobal = 400;

  // Coarsegrid solver
  PetscScalar coarse_rtol = 1.0e-8;
  PetscScalar coarse_atol = 1.0e-50;
  PetscScalar coarse_dtol = 1e3;
  PetscInt coarse_maxits = 30;
  PetscInt coarse_restart = 30;

  // Number of smoothening iterations per up/down smooth_sweeps
  PetscInt smooth_sweeps = 4;

  // Set up the solver
  ierr = KSPSetType (ksp, KSPFGMRES);  // KSPCG, KSPGMRES
  CHKERRQ (ierr);
  ierr = KSPGMRESSetRestart (ksp, restart);
  CHKERRQ (ierr);
  ierr = KSPSetTolerances (ksp, rtol, atol, dtol, maxitsGlobal);
  CHKERRQ (ierr);
  ierr = KSPSetInitialGuessNonzero (ksp, PETSC_TRUE);
  CHKERRQ (ierr);
  ierr = KSPSetOperators (ksp, K, K);
  CHKERRQ (ierr);

  // The preconditinoer
  KSPGetPC (ksp, &pc);
  // Make PCMG the default solver
  PCSetType (pc, PCMG);

  // Set solver from options
  KSPSetFromOptions (ksp);

  // Get the prec again - check if it has changed
  KSPGetPC (ksp, &pc);

  // Flag for pcmg pc
  PetscBool pcmg_flag = PETSC_TRUE;
  PetscObjectTypeCompare ((PetscObject)pc, PCMG, &pcmg_flag);

  // Only if PCMG is used
  if (pcmg_flag) {
    // DMs for grid hierachy
    DM *da_list, *daclist;
    Mat R;

    PetscMalloc (sizeof (DM) * nlvls, &da_list);
    for (PetscInt k = 0; k < nlvls; k++) da_list[k] = NULL;
    PetscMalloc (sizeof (DM) * nlvls, &daclist);
    for (PetscInt k = 0; k < nlvls; k++) daclist[k] = NULL;

    // Set 0 to the finest level
    daclist[0] = da_nodal;

    // Coordinates
    PetscReal xmin = xc[0], xmax = xc[1];
    PetscReal ymin = xc[2], ymax = xc[3];
    PetscReal zmin = xc[4], zmax = xc[5];

    // Set up the coarse meshes
    DMCoarsenHierarchy (da_nodal, nlvls - 1, &daclist[1]);
    for (PetscInt k = 0; k < nlvls; k++) {
      // NOTE: finest grid is nlevels - 1: PCMG MUST USE THIS ORDER ???
      da_list[k] = daclist[nlvls - 1 - k];
      // THIS SHOULD NOT BE NECESSARY
      DMDASetUniformCoordinates (da_list[k], xmin, xmax, ymin, ymax, zmin, zmax);
    }

    // the PCMG specific options
    PCMGSetLevels (pc, nlvls, NULL);
    PCMGSetType (pc, PC_MG_MULTIPLICATIVE);  // Default
    ierr = PCMGSetCycleType (pc, PC_MG_CYCLE_V);
    CHKERRQ (ierr);
    PCMGSetGalerkin (pc, PC_MG_GALERKIN_BOTH);
    for (PetscInt k = 1; k < nlvls; k++) {
      DMCreateInterpolation (da_list[k - 1], da_list[k], &R, NULL);
      PCMGSetInterpolation (pc, k, R);
      MatDestroy (&R);
    }

    // tidy up
    for (PetscInt k = 1; k < nlvls; k++) {  // DO NOT DESTROY LEVEL 0
      DMDestroy (&daclist[k]);
    }
    PetscFree (da_list);
    PetscFree (daclist);

    // AVOID THE DEFAULT FOR THE MG PART
    {
      // SET the coarse grid solver:
      // i.e. get a pointer to the ksp and change its settings
      KSP cksp;
      PCMGGetCoarseSolve (pc, &cksp);
      // The solver
      ierr = KSPSetType (cksp, KSPGMRES);  // KSPCG, KSPFGMRES
      ierr = KSPGMRESSetRestart (cksp, coarse_restart);
      ierr = KSPSetTolerances (cksp, coarse_rtol, coarse_atol, coarse_dtol, coarse_maxits);
      // The preconditioner
      PC cpc;
      KSPGetPC (cksp, &cpc);
      PCSetType (cpc, PCSOR);  // PCSOR, PCSPAI (NEEDS TO BE COMPILED), PCJACOBI

      // Set smoothers on all levels (except for coarse grid):
      for (PetscInt k = 1; k < nlvls; k++) {
        KSP dksp;
        PCMGGetSmoother (pc, k, &dksp);
        PC dpc;
        KSPGetPC (dksp, &dpc);
        ierr = KSPSetType (dksp, KSPGMRES);  // KSPCG, KSPGMRES, KSPCHEBYSHEV (VERY GOOD FOR SPD)
        ierr = KSPGMRESSetRestart (dksp, smooth_sweeps);
        // NOTE in the above maxitr=restart;
        ierr = KSPSetTolerances (dksp, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT, smooth_sweeps);
        PCSetType (dpc, PCSOR);  // PCJACOBI, PCSOR for KSPCHEBYSHEV very good
      }
    }
  }

  // Write check to screen:
  // Check the overall Krylov solver
  KSPType ksptype;
  KSPGetType (ksp, &ksptype);
  PCType pctype;
  PCGetType (pc, &pctype);
  PetscInt mmax;
  KSPGetTolerances (ksp, NULL, NULL, NULL, &mmax);
  PetscPrintf (PETSC_COMM_WORLD,
               "##############################################################\n");
  PetscPrintf (PETSC_COMM_WORLD,
               "################# Linear solver settings #####################\n");
  PetscPrintf (PETSC_COMM_WORLD, "# Main solver: %s, prec.: %s, maxiter.: %i \n", ksptype, pctype,
               mmax);

  // Only if pcmg is used
  if (pcmg_flag) {
    // Check the smoothers and coarse grid solver:
    for (PetscInt k = 0; k < nlvls; k++) {
      KSP dksp;
      PC dpc;
      KSPType dksptype;
      PCMGGetSmoother (pc, k, &dksp);
      KSPGetType (dksp, &dksptype);
      KSPGetPC (dksp, &dpc);
      PCType dpctype;
      PCGetType (dpc, &dpctype);
      PetscInt mmax;
      KSPGetTolerances (dksp, NULL, NULL, NULL, &mmax);
      PetscPrintf (PETSC_COMM_WORLD, "# Level %i smoother: %s, prec.: %s, sweep: %i \n", k,
                   dksptype, dpctype, mmax);
    }
  }
  PetscPrintf (PETSC_COMM_WORLD,
               "##############################################################\n");

  return (ierr);
}


PetscErrorCode LinearElasticity ::DMDAGetElements_3D (DM dm, PetscInt *nel, PetscInt *nen,
                                                      const PetscInt *e[]) {
  PetscErrorCode ierr;
  DM_DA *da = (DM_DA *)dm->data;
  PetscInt i, xs, xe, Xs, Xe;
  PetscInt j, ys, ye, Ys, Ye;
  PetscInt k, zs, ze, Zs, Ze;
  PetscInt cnt = 0, cell[8], ns = 1, nn = 8;
  PetscInt c;

  if (!da->e) {
    if (da->elementtype == DMDA_ELEMENT_Q1) {
      ns = 1;
      nn = 8;
    }
    ierr = DMDAGetCorners (dm, &xs, &ys, &zs, &xe, &ye, &ze);
    CHKERRQ (ierr);
    ierr = DMDAGetGhostCorners (dm, &Xs, &Ys, &Zs, &Xe, &Ye, &Ze);
    CHKERRQ (ierr);
    xe += xs;
    Xe += Xs;
    if (xs != Xs) xs -= 1;
    ye += ys;
    Ye += Ys;
    if (ys != Ys) ys -= 1;
    ze += zs;
    Ze += Zs;
    if (zs != Zs) zs -= 1;
    da->ne = ns * (xe - xs - 1) * (ye - ys - 1) * (ze - zs - 1);
    PetscMalloc ((1 + nn * da->ne) * sizeof (PetscInt), &da->e);

    for (k = zs; k < ze - 1; k++) {
      for (j = ys; j < ye - 1; j++) {
        for (i = xs; i < xe - 1; i++) {
          cell[0] = (i - Xs) + (j - Ys) * (Xe - Xs) + (k - Zs) * (Xe - Xs) * (Ye - Ys);
          cell[1] = (i - Xs + 1) + (j - Ys) * (Xe - Xs) + (k - Zs) * (Xe - Xs) * (Ye - Ys);
          cell[2] = (i - Xs + 1) + (j - Ys + 1) * (Xe - Xs) + (k - Zs) * (Xe - Xs) * (Ye - Ys);
          cell[3] = (i - Xs) + (j - Ys + 1) * (Xe - Xs) + (k - Zs) * (Xe - Xs) * (Ye - Ys);
          cell[4] = (i - Xs) + (j - Ys) * (Xe - Xs) + (k - Zs + 1) * (Xe - Xs) * (Ye - Ys);
          cell[5] = (i - Xs + 1) + (j - Ys) * (Xe - Xs) + (k - Zs + 1) * (Xe - Xs) * (Ye - Ys);
          cell[6] = (i - Xs + 1) + (j - Ys + 1) * (Xe - Xs) + (k - Zs + 1) * (Xe - Xs) * (Ye - Ys);
          cell[7] = (i - Xs) + (j - Ys + 1) * (Xe - Xs) + (k - Zs + 1) * (Xe - Xs) * (Ye - Ys);
          if (da->elementtype == DMDA_ELEMENT_Q1) {
            for (c = 0; c < ns * nn; c++) da->e[cnt++] = cell[c];
          }
        }
      }
    }
  }

  *nel = da->ne;
  *nen = nn;
  *e = da->e;

  return (0);
}


PetscInt LinearElasticity ::Hex8Isoparametric (PetscScalar *X, PetscScalar *Y, PetscScalar *Z,
                                               PetscScalar nu, PetscInt redInt, PetscScalar *ke) {
  // HEX8_ISOPARAMETRIC - Computes HEX8 isoparametric element matrices
  // The element stiffness matrix is computed as:
  //
  //       ke = int(int(int(B^T*C*B,x),y),z)
  //
  // For an isoparameteric element this integral becomes:
  //
  //       ke = int(int(int(B^T*C*B*det(J),xi=-1..1),eta=-1..1),zeta=-1..1)
  //
  // where B is the more complicated expression:
  // B = [dx*alpha1 + dy*alpha2 + dz*alpha3]*N
  // where
  // dx = [invJ11 invJ12 invJ13]*[dxi deta dzeta]
  // dy = [invJ21 invJ22 invJ23]*[dxi deta dzeta]
  // dy = [invJ31 invJ32 invJ33]*[dxi deta dzeta]
  //
  // Remark: The elasticity modulus is left out in the below
  // computations, because we multiply with it afterwards (the aim is
  // topology optimization).
  // Furthermore, this is not the most efficient code, but it is readable.
  //
  /////////////////////////////////////////////////////////////////////////////////
  //////// INPUT:
  // X, Y, Z  = Vectors containing the coordinates of the eight nodes
  //               (x1,y1,z1,x2,y2,z2,...,x8,y8,z8). Where node 1 is in the lower
  //               left corner, and node 2 is the next node counterclockwise
  //               (looking in the negative z-dir).
  //               Finish the x-y-plane and then move in the positive z-dir.
  // redInt   = Reduced integration option boolean (here an integer).
  //           	redInt == 0 (false): Full integration
  //           	redInt == 1 (true): Reduced integration
  // nu 		= Poisson's ratio.
  //
  //////// OUTPUT:
  // ke  = Element stiffness matrix. Needs to be multiplied with elasticity modulus
  //
  //   Written 2013 at
  //   Department of Mechanical Engineering
  //   Technical University of Denmark (DTU).
  /////////////////////////////////////////////////////////////////////////////////

  //// COMPUTE ELEMENT STIFFNESS MATRIX
  // Lame's parameters (with E=1.0):
  PetscScalar lambda = nu / ((1.0 + nu) * (1.0 - 2.0 * nu));
  PetscScalar mu = 1.0 / (2.0 * (1.0 + nu));
  // Constitutive matrix
  PetscScalar C[6][6] = {{lambda + 2.0 * mu, lambda, lambda, 0.0, 0.0, 0.0},
                         {lambda, lambda + 2.0 * mu, lambda, 0.0, 0.0, 0.0},
                         {lambda, lambda, lambda + 2.0 * mu, 0.0, 0.0, 0.0},
                         {0.0, 0.0, 0.0, mu, 0.0, 0.0},
                         {0.0, 0.0, 0.0, 0.0, mu, 0.0},
                         {0.0, 0.0, 0.0, 0.0, 0.0, mu}};
  // V-matrix for computing von Mises stress
  PetscScalar VMatrix[6][6] = {{1.0, -0.5, -0.5, 0.0, 0.0, 0.0}, {-0.5, 1.0, -0.5, 0.0, 0.0, 0.0},
                               {-0.5, -0.5, 1.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 3.0, 0.0, 0.0},
                               {0.0, 0.0, 0.0, 0.0, 3.0, 0.0},   {0.0, 0.0, 0.0, 0.0, 0.0, 3.0}};

  // define C private (as a std::vector<int>)
  CPrivate.resize (6);
  for (int i = 0; i < 6; i++) {
    CPrivate[i].resize (6);
    for (int j = 0; j < 6; j++) CPrivate[i][j] = C[i][j];
  }

  // Gauss points (GP) and weigths
  // Two Gauss points in all directions (total of eight)
  PetscScalar GP[2] = {-0.577350269189626, 0.577350269189626};
  // Corresponding weights
  PetscScalar W[2] = {1.0, 1.0};
  // If reduced integration only use one GP
  if (redInt) {
    GP[0] = 0.0;
    W[0] = 2.0;
  }
  // Matrices that help when we gather the strain-displacement matrix:
  PetscScalar alpha1[6][3];
  PetscScalar alpha2[6][3];
  PetscScalar alpha3[6][3];
  memset (alpha1, 0, sizeof (alpha1[0][0]) * 6 * 3);  // zero out
  memset (alpha2, 0, sizeof (alpha2[0][0]) * 6 * 3);  // zero out
  memset (alpha3, 0, sizeof (alpha3[0][0]) * 6 * 3);  // zero out
  alpha1[0][0] = 1.0;
  alpha1[3][1] = 1.0;
  alpha1[5][2] = 1.0;
  alpha2[1][1] = 1.0;
  alpha2[3][0] = 1.0;
  alpha2[4][2] = 1.0;
  alpha3[2][2] = 1.0;
  alpha3[4][1] = 1.0;
  alpha3[5][0] = 1.0;
  PetscScalar dNdxi[8];
  PetscScalar dNdeta[8];
  PetscScalar dNdzeta[8];
  PetscScalar J[3][3];
  PetscScalar invJ[3][3];
  PetscScalar beta[6][3];
  PetscScalar B[6][24];  // Note: Small enough to be allocated on stack
  PetscScalar *dN;
  // Make sure the stiffness matrix is zeroed out:
  memset (ke, 0, sizeof (ke[0]) * 24 * 24);
  fThhermalElem.resize (24, 0.0);  // elemental thermal force

  // Perform the numerical integration
  for (PetscInt ii = 0; ii < 2 - redInt; ii++) {
    for (PetscInt jj = 0; jj < 2 - redInt; jj++) {
      for (PetscInt kk = 0; kk < 2 - redInt; kk++) {
        // Integration point
        PetscScalar xi = GP[ii];
        PetscScalar eta = GP[jj];
        PetscScalar zeta = GP[kk];
        // Differentiated shape functions
        DifferentiatedShapeFunctions (xi, eta, zeta, dNdxi, dNdeta, dNdzeta);
        // Jacobian
        J[0][0] = Dot (dNdxi, X, 8);
        J[0][1] = Dot (dNdxi, Y, 8);
        J[0][2] = Dot (dNdxi, Z, 8);
        J[1][0] = Dot (dNdeta, X, 8);
        J[1][1] = Dot (dNdeta, Y, 8);
        J[1][2] = Dot (dNdeta, Z, 8);
        J[2][0] = Dot (dNdzeta, X, 8);
        J[2][1] = Dot (dNdzeta, Y, 8);
        J[2][2] = Dot (dNdzeta, Z, 8);
        // Inverse and determinant
        PetscScalar detJ = Inverse3M (J, invJ);
        // Weight factor at this point
        PetscScalar weight = W[ii] * W[jj] * W[kk] * detJ;
        // Strain-displacement matrix
        memset (B, 0, sizeof (B[0][0]) * 6 * 24);  // zero out

        for (PetscInt ll = 0; ll < 3; ll++) {
          // Add contributions from the different derivatives
          if (ll == 0) {
            dN = dNdxi;
          }
          if (ll == 1) {
            dN = dNdeta;
          }
          if (ll == 2) {
            dN = dNdzeta;
          }
          // Assemble strain operator
          for (PetscInt i = 0; i < 6; i++) {
            for (PetscInt j = 0; j < 3; j++) {
              beta[i][j] = invJ[0][ll] * alpha1[i][j] + invJ[1][ll] * alpha2[i][j]
                           + invJ[2][ll] * alpha3[i][j];
            }
          }
          // Add contributions to strain-displacement matrix
          for (PetscInt i = 0; i < 6; i++) {
            for (PetscInt j = 0; j < 24; j++) {
              B[i][j] = B[i][j] + beta[i][j % 3] * dN[j / 3];
            }
          }
        }

        // BMatrixVector is used to compute stress at Gauss points
        std::vector<std::vector<double>> Btemp;
        Btemp.resize (6);
        for (PetscInt i = 0; i < 6; i++) {
          Btemp[i].resize (24);
          for (PetscInt j = 0; j < 24; j++) {
            Btemp[i][j] = B[i][j];
          }
        }
        BMatrixVector.push_back (Btemp);

        std::vector<std::vector<double>> VCBTemp;
        VCBTemp.resize (6);
        for (PetscInt i = 0; i < 6; i++) VCBTemp[i].resize (24, 0.0);

        for (PetscInt i = 0; i < 6; i++) {
          for (PetscInt j = 0; j < 6; j++) {
            for (PetscInt k = 0; k < 6; k++) {
              for (PetscInt l = 0; l < 24; l++) {
                VCBTemp[i][l] += VMatrix[i][j] * C[j][k] * Btemp[k][l];
              }
            }
          }
        }
        VCBVector.push_back (VCBTemp);

        // Finally, add to the element matrix
        for (PetscInt i = 0; i < 24; i++) {
          for (PetscInt j = 0; j < 24; j++) {
            for (PetscInt k = 0; k < 6; k++) {
              for (PetscInt l = 0; l < 6; l++) {
                ke[j + 24 * i] = ke[j + 24 * i] + weight * (B[k][i] * C[k][l] * B[l][j]);
              }
            }  // k
          }    // j
        }      // i

        // compute CE (C * epsilon_th) here
        PetscScalar CE[6]
            = {3 * lambda + 2 * mu, 3 * lambda + 2 * mu, 3 * lambda + 2 * mu, 0.0, 0.0, 0.0};

        // now compute Bt*CE here
        for (PetscInt i = 0; i < 6; i++) {
          for (PetscInt j = 0; j < 24; j++) {
            fThhermalElem[j] += Btemp[i][j] * CE[i] * weight;
          }
        }
      }
    }
  }

  return 0;
}


std::vector<double> LinearElasticity ::Hex8Isoparametric (PetscScalar *X, PetscScalar *Y,
                                                          PetscScalar *Z, PetscScalar nu,
                                                          PetscInt redInt,
                                                          std::vector<double> quadratureWeights) {
  // assemble stiffness matrix assuming a given quadrature points and custom
  // weights. quadratureWeights should be defined in [-1,1] coordinate system

  //// COMPUTE ELEMENT STIFFNESS MATRIX
  // Lame's parameters (with E=1.0):
  PetscScalar lambda = nu / ((1.0 + nu) * (1.0 - 2.0 * nu));
  PetscScalar mu = 1.0 / (2.0 * (1.0 + nu));
  // Constitutive matrix
  PetscScalar C[6][6] = {{lambda + 2.0 * mu, lambda, lambda, 0.0, 0.0, 0.0},
                         {lambda, lambda + 2.0 * mu, lambda, 0.0, 0.0, 0.0},
                         {lambda, lambda, lambda + 2.0 * mu, 0.0, 0.0, 0.0},
                         {0.0, 0.0, 0.0, mu, 0.0, 0.0},
                         {0.0, 0.0, 0.0, 0.0, mu, 0.0},
                         {0.0, 0.0, 0.0, 0.0, 0.0, mu}};
  // V-matrix for computing von Mises stress
  PetscScalar VMatrix[6][6] = {{1.0, -0.5, -0.5, 0.0, 0.0, 0.0}, {-0.5, 1.0, -0.5, 0.0, 0.0, 0.0},
                               {-0.5, -0.5, 1.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 3.0, 0.0, 0.0},
                               {0.0, 0.0, 0.0, 0.0, 3.0, 0.0},   {0.0, 0.0, 0.0, 0.0, 0.0, 3.0}};

  // Gauss points (GP) and weigths
  // Two Gauss points in all directions (total of eight)
  PetscScalar GP[3] = {-std::sqrt (0.6), 0.0, std::sqrt (0.6)};
  int numGaussPointsPerDim = 3;

  // Matrices that help when we gather the strain-displacement matrix:
  PetscScalar alpha1[6][3];
  PetscScalar alpha2[6][3];
  PetscScalar alpha3[6][3];
  memset (alpha1, 0, sizeof (alpha1[0][0]) * 6 * 3);  // zero out
  memset (alpha2, 0, sizeof (alpha2[0][0]) * 6 * 3);  // zero out
  memset (alpha3, 0, sizeof (alpha3[0][0]) * 6 * 3);  // zero out
  alpha1[0][0] = 1.0;
  alpha1[3][1] = 1.0;
  alpha1[5][2] = 1.0;
  alpha2[1][1] = 1.0;
  alpha2[3][0] = 1.0;
  alpha2[4][2] = 1.0;
  alpha3[2][2] = 1.0;
  alpha3[4][1] = 1.0;
  alpha3[5][0] = 1.0;
  PetscScalar dNdxi[8];
  PetscScalar dNdeta[8];
  PetscScalar dNdzeta[8];
  PetscScalar J[3][3];
  PetscScalar invJ[3][3];
  PetscScalar beta[6][3];
  PetscScalar B[6][24];  // Note: Small enough to be allocated on stack
  PetscScalar *dN;
  // Make sure the stiffness matrix is zeroed out:
  std::vector<double> ke (24 * 24, 0.0);

  if (!isBMatrixVectorComputed) {
    BMatrixVector.resize (0);
  }
  if (!isVCBVectorComputed) {
    VCBVector.resize (0);
  }

  // Perform the numerical integration
  for (PetscInt kk = 0; kk < numGaussPointsPerDim; kk++) {
    for (PetscInt jj = 0; jj < numGaussPointsPerDim; jj++) {
      for (PetscInt ii = 0; ii < numGaussPointsPerDim; ii++) {
        // Integration point
        PetscScalar xi = GP[ii];
        PetscScalar eta = GP[jj];
        PetscScalar zeta = GP[kk];
        // Differentiated shape functions
        DifferentiatedShapeFunctions (xi, eta, zeta, dNdxi, dNdeta, dNdzeta);
        // Jacobian
        J[0][0] = Dot (dNdxi, X, 8);
        J[0][1] = Dot (dNdxi, Y, 8);
        J[0][2] = Dot (dNdxi, Z, 8);
        J[1][0] = Dot (dNdeta, X, 8);
        J[1][1] = Dot (dNdeta, Y, 8);
        J[1][2] = Dot (dNdeta, Z, 8);
        J[2][0] = Dot (dNdzeta, X, 8);
        J[2][1] = Dot (dNdzeta, Y, 8);
        J[2][2] = Dot (dNdzeta, Z, 8);
        // Inverse and determinant
        PetscScalar detJ = Inverse3M (J, invJ);
        // Weight factor at this point
        // PetscScalar weight = W[ii]*W[jj]*W[kk]*detJ;
        PetscScalar weight = quadratureWeights[ii + jj * numGaussPointsPerDim
                                               + kk * numGaussPointsPerDim * numGaussPointsPerDim]
                             * detJ;

        // Strain-displacement matrix
        memset (B, 0, sizeof (B[0][0]) * 6 * 24);  // zero out

        for (PetscInt ll = 0; ll < 3; ll++) {
          // Add contributions from the different derivatives
          if (ll == 0) {
            dN = dNdxi;
          }
          if (ll == 1) {
            dN = dNdeta;
          }
          if (ll == 2) {
            dN = dNdzeta;
          }
          // Assemble strain operator
          for (PetscInt i = 0; i < 6; i++) {
            for (PetscInt j = 0; j < 3; j++) {
              beta[i][j] = invJ[0][ll] * alpha1[i][j] + invJ[1][ll] * alpha2[i][j]
                           + invJ[2][ll] * alpha3[i][j];
            }
          }
          // Add contributions to strain-displacement matrix
          for (PetscInt i = 0; i < 6; i++) {
            for (PetscInt j = 0; j < 24; j++) {
              B[i][j] = B[i][j] + beta[i][j % 3] * dN[j / 3];
            }
          }
        }

        // BMatrixVector is used to compute stress at Gauss points
        if (!isBMatrixVectorComputed) {
          std::vector<std::vector<double>> Btemp;
          Btemp.resize (6);
          for (PetscInt i = 0; i < 6; i++) {
            Btemp[i].resize (24);
            for (PetscInt j = 0; j < 24; j++) {
              Btemp[i][j] = B[i][j];
            }
          }
          BMatrixVector.push_back (Btemp);
        }

        if (!isVCBVectorComputed) {
          std::vector<std::vector<double>> VCBTemp;
          VCBTemp.resize (6);
          for (PetscInt i = 0; i < 6; i++) VCBTemp[i].resize (24, 0.0);

          for (PetscInt i = 0; i < 6; i++) {
            for (PetscInt j = 0; j < 6; j++) {
              for (PetscInt k = 0; k < 6; k++) {
                for (PetscInt l = 0; l < 24; l++) {
                  VCBTemp[i][l] += VMatrix[i][j] * C[j][k] * B[k][l];
                }
              }
            }
          }
          VCBVector.push_back (VCBTemp);
        }

        // Finally, add to the element matrix
        for (PetscInt i = 0; i < 24; i++) {
          for (PetscInt j = 0; j < 24; j++) {
            for (PetscInt k = 0; k < 6; k++) {
              for (PetscInt l = 0; l < 6; l++) {
                ke[j + 24 * i] = ke[j + 24 * i] + weight * (B[k][i] * C[k][l] * B[l][j]);
              }
            }  // k
          }    // j
        }      // i
      }
    }
  }

  isBMatrixVectorComputed = true;
  isVCBVectorComputed = true;
  return ke;
}


PetscScalar LinearElasticity ::Dot (PetscScalar *v1, PetscScalar *v2, PetscInt l) {
  // Function that returns the dot product of v1 and v2,
  // which must have the same length l
  PetscScalar result = 0.0;
  for (PetscInt i = 0; i < l; i++) {
    result = result + v1[i] * v2[i];
  }

  return result;
}


void LinearElasticity ::DifferentiatedShapeFunctions (PetscScalar xi, PetscScalar eta,
                                                      PetscScalar zeta, PetscScalar *dNdxi,
                                                      PetscScalar *dNdeta, PetscScalar *dNdzeta) {
  // differentiatedShapeFunctions - Computes differentiated shape functions
  // At the point given by (xi, eta, zeta).
  // With respect to xi:
  dNdxi[0] = -0.125 * (1.0 - eta) * (1.0 - zeta);
  dNdxi[1] = 0.125 * (1.0 - eta) * (1.0 - zeta);
  dNdxi[2] = 0.125 * (1.0 + eta) * (1.0 - zeta);
  dNdxi[3] = -0.125 * (1.0 + eta) * (1.0 - zeta);
  dNdxi[4] = -0.125 * (1.0 - eta) * (1.0 + zeta);
  dNdxi[5] = 0.125 * (1.0 - eta) * (1.0 + zeta);
  dNdxi[6] = 0.125 * (1.0 + eta) * (1.0 + zeta);
  dNdxi[7] = -0.125 * (1.0 + eta) * (1.0 + zeta);
  // With respect to eta:
  dNdeta[0] = -0.125 * (1.0 - xi) * (1.0 - zeta);
  dNdeta[1] = -0.125 * (1.0 + xi) * (1.0 - zeta);
  dNdeta[2] = 0.125 * (1.0 + xi) * (1.0 - zeta);
  dNdeta[3] = 0.125 * (1.0 - xi) * (1.0 - zeta);
  dNdeta[4] = -0.125 * (1.0 - xi) * (1.0 + zeta);
  dNdeta[5] = -0.125 * (1.0 + xi) * (1.0 + zeta);
  dNdeta[6] = 0.125 * (1.0 + xi) * (1.0 + zeta);
  dNdeta[7] = 0.125 * (1.0 - xi) * (1.0 + zeta);
  // With respect to zeta:
  dNdzeta[0] = -0.125 * (1.0 - xi) * (1.0 - eta);
  dNdzeta[1] = -0.125 * (1.0 + xi) * (1.0 - eta);
  dNdzeta[2] = -0.125 * (1.0 + xi) * (1.0 + eta);
  dNdzeta[3] = -0.125 * (1.0 - xi) * (1.0 + eta);
  dNdzeta[4] = 0.125 * (1.0 - xi) * (1.0 - eta);
  dNdzeta[5] = 0.125 * (1.0 + xi) * (1.0 - eta);
  dNdzeta[6] = 0.125 * (1.0 + xi) * (1.0 + eta);
  dNdzeta[7] = 0.125 * (1.0 - xi) * (1.0 + eta);
}


PetscScalar LinearElasticity ::Inverse3M (PetscScalar J[][3], PetscScalar invJ[][3]) {
  // inverse3M - Computes the inverse of a 3x3 matrix
  PetscScalar detJ = J[0][0] * (J[1][1] * J[2][2] - J[2][1] * J[1][2])
                     - J[0][1] * (J[1][0] * J[2][2] - J[2][0] * J[1][2])
                     + J[0][2] * (J[1][0] * J[2][1] - J[2][0] * J[1][1]);

  invJ[0][0] = (J[1][1] * J[2][2] - J[2][1] * J[1][2]) / detJ;
  invJ[0][1] = -(J[0][1] * J[2][2] - J[0][2] * J[2][1]) / detJ;
  invJ[0][2] = (J[0][1] * J[1][2] - J[0][2] * J[1][1]) / detJ;
  invJ[1][0] = -(J[1][0] * J[2][2] - J[1][2] * J[2][0]) / detJ;
  invJ[1][1] = (J[0][0] * J[2][2] - J[0][2] * J[2][0]) / detJ;
  invJ[1][2] = -(J[0][0] * J[1][2] - J[0][2] * J[1][0]) / detJ;
  invJ[2][0] = (J[1][0] * J[2][1] - J[1][1] * J[2][0]) / detJ;
  invJ[2][1] = -(J[0][0] * J[2][1] - J[0][1] * J[2][0]) / detJ;
  invJ[2][2] = (J[0][0] * J[1][1] - J[1][0] * J[0][1]) / detJ;

  return detJ;
}


// Added by Sandy for Stress constraints; borrowed the code from Oded;
// modified for LS method by Sandy
PetscErrorCode LinearElasticity ::ComputeStressSensitivitiesSandy (PetscScalar *fx, Vec dfdx,
                                                                   Vec xPhys, Vec ignoreElemStress,
                                                                   PetscScalar Emin,
                                                                   PetscScalar Emax,
                                                                   PetscScalar penal) {
  // Error code
  PetscErrorCode ierr;

  // NOTE: Sandy made this change
  PetscScalar qnorm = 2.0;
  PetscScalar pnorm = 6.0;
  PetscScalar penalsig = qnorm / pnorm;

  // Solve state eqs
  ierr = SolveState (xPhys, Emin, Emax, penal);
  CHKERRQ (ierr);

  // Get the FE mesh structure (from the nodal mesh)
  PetscInt nel, nen;
  const PetscInt *necon;
  ierr = DMDAGetElements_3D (da_nodal, &nel, &nen, &necon);
  CHKERRQ (ierr);

  // Get pointer to the densities
  PetscScalar *xp;
  VecGetArray (xPhys, &xp);

  // Get Solution
  Vec Uloc;
  DMCreateLocalVector (da_nodal, &Uloc);
  DMGlobalToLocalBegin (da_nodal, U, INSERT_VALUES, Uloc);
  DMGlobalToLocalEnd (da_nodal, U, INSERT_VALUES, Uloc);

  // get pointer to local vector
  PetscScalar *up;
  VecGetArray (Uloc, &up);

  // Prepare vectors for adjoint solution
  Vec LAMloc;
  PetscScalar *lamp;

  ierr = DMCreateLocalVector (da_nodal, &LAMloc);
  CHKERRQ (ierr);

  VecSet (dfdx, 0.0);
  PetscScalar *df3;
  VecGetArray (dfdx, &df3);

  PetscScalar *ignoreElemStress_p;
  VecGetArray (ignoreElemStress, &ignoreElemStress_p);

  // edof array
  PetscInt edof[24];

  // Zeroize adjoint load
  VecSet (adRHS, 0.0);

  PetscScalar sumsigma = 0.0;    // For accumulation sigma^pnorm
  PetscScalar maxsigma = 0.0;    // For computing actual max
  PetscScalar maxsigmaPN = 0.0;  // For computing approximate max

  // Loop through gauss points
  for (PetscInt gp = 0; gp < 8; gp++) {
    // Loop through elements
    for (PetscInt e = 0; e < nel; e++) {
      std::vector<std::vector<double>> BMatrixLocal = BMatrixVector[gp];
      std::vector<std::vector<double>> VCBLocal = VCBVector[gp];

      // Loop over element nodes
      for (PetscInt j = 0; j < nen; j++) {
        // Get local dofs
        for (PetscInt k = 0; k < 3; k++) {
          edof[j * 3 + k] = 3 * necon[e * nen + j] + k;
        }
      }

      PetscScalar sigma[6];

      for (PetscInt j = 0; j < 6; j++) {
        sigma[j] = 0.0;
      }
      PetscScalar vms2 = 0.0;
      PetscScalar vms = 0.0;
      PetscScalar vm = 0.0;
      PetscScalar dEdxp = 0.0;
      PetscScalar dsigdxp = 0.0;

      // Compute stress as C*B*u for E=1
      for (PetscInt j = 0; j < 6; j++) {
        sigma[j] = 0.0;
        for (PetscInt k = 0; k < 6; k++) {
          for (PetscInt l = 0; l < 24; l++) {
            sigma[j] += CPrivate[j][k] * BMatrixLocal[k][l] * up[edof[l]];
          }
        }
      }

      // Compute von Mises stress for E=1
      // First compute sigma_{VM}^2
      vms2 = std::pow (sigma[0] - sigma[1], 2.0) + std::pow (sigma[1] - sigma[2], 2.0)
             + std::pow (sigma[2] - sigma[0], 2.0)
             + (6.0 * (std::pow (sigma[3], 2.0)) + 6.0 * (std::pow (sigma[4], 2.0))
                + 6.0 * (std::pow (sigma[5], 2.0)));
      vms2 = vms2 * 0.5;

      // Then take the square root, this gives VMS stress for E=1
      vms = PetscPowScalar (vms2, 0.5);
      vm = (PetscPowScalar (xp[e], penalsig) * (Emax)) * vms;

      // only the elements that are not ignored
      if (ignoreElemStress_p[e] == 0.0) {  // do nothing
      } else {
        vms = 0.0;
        vm = 0.0;
      }

      // Add the element stress to the approximate max
      sumsigma += PetscPowScalar (vm, pnorm);

      // Update the true max
      maxsigma = PetscMax (vm, maxsigma);

      // Compute SVCB of the element
      PetscScalar SVCB[24];  // For sigma'*V*C*B
      for (PetscInt j = 0; j < 24; j++) {
        SVCB[j] = 0.0;
        for (PetscInt k = 0; k < 6; k++) {
          SVCB[j] += VCBLocal[k][j] * sigma[k];
        }
      }

      // Compute contribution to adRHS and to dsig/dx
      PetscScalar adRHSe[24];
      for (PetscInt j = 0; j < 24; j++) {
        // only the elements that are not ignored
        if (ignoreElemStress_p[e] == 0.0) {
          adRHSe[j] = -PetscPowScalar (xp[e], qnorm) * (pnorm)*PetscPowScalar (vms, pnorm - 2.0)
                      * SVCB[j];
        } else {
          adRHSe[j] = 0.0;
        }
      }

      // Assemble into adRHS vector
      ierr = VecSetValuesLocal (adRHS, 24, edof, adRHSe, ADD_VALUES);
      CHKERRQ (ierr);  // bookmark

      // Compute derivative dsigmamax/dx
      PetscScalar tmpxp = PetscMax (1.0e-6, xp[e]);
      PetscScalar tmpgrad = PetscPowScalar (tmpxp, penalsig);
      tmpgrad = 1.0 / tmpgrad;
      dEdxp = penalsig * tmpgrad * (Emax - Emin);
      dsigdxp = dEdxp * vms * pnorm * PetscPowScalar (vm, pnorm - 1.0);
      // df1[e] = dsigdxp; // Store temporarily in dg (this is not the final sensitivity) NOTE:
      // Sandy replaced dg with df1

      // only the elements that are not ignored
      if (ignoreElemStress_p[e] == 0.0) {
        // NOTE: Sandy made changes ; using LS formulation
        df3[e] += PetscPowScalar (xp[e], (qnorm - 1)) * (qnorm)*PetscPowScalar (vms, pnorm);
      } else {
        df3[e] += 0.0;
      }
    }  // end of elements
  }    // end of gauss points

  // Sum up and compute approximate stress value
  PetscScalar tmp = sumsigma;
  sumsigma = 0.0;
  MPI_Allreduce (&tmp, &(sumsigma), 1, MPIU_SCALAR, MPI_SUM, PETSC_COMM_WORLD);
  maxsigmaPN = PetscPowScalar (sumsigma, 1.0 / pnorm);
  tmp = maxsigma;
  maxsigma = 0.0;
  MPI_Allreduce (&tmp, &(maxsigma), 1, MPIU_SCALAR, MPI_MAX, PETSC_COMM_WORLD);

  // Print approx and true max stress
  PetscPrintf (PETSC_COMM_WORLD, "Appr max stress is %f \n", maxsigmaPN);
  PetscPrintf (PETSC_COMM_WORLD, "True max stress is %f \n", maxsigma);

  MPI_Barrier (PETSC_COMM_WORLD);

  // Set max p-norm stress to objective
  fx[0] = maxsigmaPN;

  // Assemble and scale adjoint load
  VecAssemblyBegin (adRHS);
  VecAssemblyEnd (adRHS);
  PetscScalar scaleadRHS = 1.0 / pnorm * PetscPowScalar (sumsigma, 1.0 / pnorm - 1.0);
  ierr = VecScale (adRHS, scaleadRHS);
  CHKERRQ (ierr);

  // Scale sensitivity without restoring the vector
  for (PetscInt e = 0; e < nel; e++) {
    df3[e] = df3[e] * scaleadRHS;  // NOTE: Sandy replaced dg with df
  }

  // Solve adjoint for this loadcase/stress constraint
  double t1, t2;
  t1 = MPI_Wtime ();
  PetscInt niter;
  PetscScalar rnorm;
  PetscScalar rnorm_adRHS;
  // Zero out possible loads in the adRHS that coincide with Dirichlet conditions
  VecPointwiseMult (adRHS, adRHS, N);
  // Solve
  ierr = KSPSolve (ksp, adRHS, adU);
  CHKERRQ (ierr);
  // Get iteration number and residual from KSP
  KSPGetIterationNumber (ksp, &niter);
  KSPGetResidualNorm (ksp, &rnorm);
  VecNorm (adRHS, NORM_2, &rnorm_adRHS);
  t2 = MPI_Wtime ();
  PetscPrintf (PETSC_COMM_WORLD,
               "Adjoint (stress) solver, BC %i LC %i SC %i:  iter: %i, rerr.: %e, time: %f\n", 0,
               0 + 1, 0 + 1, niter, rnorm / rnorm_adRHS, t2 - t1);

  // Get pointer to adjoint solution
  VecSet (LAMloc, 0.0);
  DMGlobalToLocalBegin (da_nodal, adU, INSERT_VALUES, LAMloc);
  DMGlobalToLocalEnd (da_nodal, adU, INSERT_VALUES, LAMloc);
  VecGetArray (LAMloc, &lamp);

  // Compute local contributions to sensitivity and sum with dsigdxdil
  for (PetscInt e = 0; e < nel; e++) {
    // Loop over element nodes
    for (PetscInt j = 0; j < nen; j++) {
      // Get local dofs
      for (PetscInt k = 0; k < 3; k++) {
        edof[j * 3 + k] = 3 * necon[e * nen + j] + k;
      }
    }

    PetscScalar lamKu = 0.0;
    PetscScalar uKu = 0.0;

    for (PetscInt k = 0; k < 24; k++) {
      for (PetscInt h = 0; h < 24; h++) {
        lamKu += lamp[edof[k]] * KE[k * 24 + h] * up[edof[h]];
        uKu += up[edof[k]] * KE[k * 24 + h] * up[edof[h]];
      }
    }
    df3[e] += lamKu * PetscPowScalar (xp[e], penal - 1.0) * (Emax - Emin);
  }

  // Restore everything
  VecRestoreArray (Uloc, &up);
  VecDestroy (&Uloc);

  VecRestoreArray (LAMloc, &lamp);
  VecDestroy (&LAMloc);

  VecRestoreArray (dfdx, &df3);
  VecRestoreArray (xPhys, &xp);
  VecRestoreArray (ignoreElemStress, &ignoreElemStress_p);

  DMDARestoreElements (da_nodal, &nel, &nen, &necon);

  return (ierr);
}


// Added by Sandy for Stress constraints; borrowed the code from Oded;
// modified for LS method by Sandy
PetscErrorCode LinearElasticity ::ComputeStressSensitivitiesFromQuadrature (
    PetscScalar *fx, Vec dfdx, Vec xPhys, Vec ignoreElemStress, std::vector<Vec> momQuad,
    PetscScalar Emin, PetscScalar Emax, PetscScalar penal) {
  // Error code
  PetscErrorCode ierr;

  // NOTE: Sandy made this change
  PetscScalar qnorm = 2.0;
  PetscScalar pnorm = 16.0;
  double halfpNorm = pnorm / 2.0;
  PetscScalar penalsig = qnorm / pnorm;

  // Solve state eqs
  ierr = SolveState (xPhys, momQuad, Emin, Emax, penal);
  CHKERRQ (ierr);

  // Get the FE mesh structure (from the nodal mesh)
  PetscInt nel, nen;
  const PetscInt *necon;
  ierr = DMDAGetElements_3D (da_nodal, &nel, &nen, &necon);
  CHKERRQ (ierr);

  // Get pointer to the densities
  PetscScalar *xp;
  VecGetArray (xPhys, &xp);

  // Get pointer to quadrature
  std::vector<PetscScalar *> momQuadPointer (27);
  for (int i = 0; i < 27; i++) {
    VecGetArray (momQuad[i], &momQuadPointer[i]);
  }

  // Get Solution
  Vec Uloc;
  DMCreateLocalVector (da_nodal, &Uloc);
  DMGlobalToLocalBegin (da_nodal, U, INSERT_VALUES, Uloc);
  DMGlobalToLocalEnd (da_nodal, U, INSERT_VALUES, Uloc);

  // get pointer to local vector
  PetscScalar *up;
  VecGetArray (Uloc, &up);

  // Prepare vectors for adjoint solution
  Vec LAMloc;
  PetscScalar *lamp;

  ierr = DMCreateLocalVector (da_nodal, &LAMloc);
  CHKERRQ (ierr);

  VecSet (dfdx, 0.0);
  PetscScalar *df3;
  VecGetArray (dfdx, &df3);

  PetscScalar *ignoreElemStress_p;
  VecGetArray (ignoreElemStress, &ignoreElemStress_p);

  // edof array
  PetscInt edof[24];
  PetscScalar ke[24 * 24];  // stiffness matrix elemental

  // Zeroize adjoint load
  VecSet (adRHS, 0.0);

  PetscScalar sumsigma = 0.0;    // For accumulation sigma^pnorm
  PetscScalar maxsigma = 0.0;    // For computing actual max
  PetscScalar maxsigmaPN = 0.0;  // For computing approximate max

  int nQuad = 27;
  double quadSumMin = 0.25;  // only consider elements that have quadsum > quadSumMin

  // Set the local stiffness matrix
  PetscScalar dx = (xc[1] - xc[0]) / (PetscScalar (ne[0]));  // x-side length
  PetscScalar dy = (xc[3] - xc[2]) / (PetscScalar (ne[1]));  // y-side length
  PetscScalar dz = (xc[5] - xc[4]) / (PetscScalar (ne[2]));  // z-side length
  PetscScalar X[8] = {0.0, dx, dx, 0.0, 0.0, dx, dx, 0.0};
  PetscScalar Y[8] = {0.0, 0.0, dy, dy, 0.0, 0.0, dy, dy};
  PetscScalar Z[8] = {0.0, 0.0, 0.0, 0.0, dz, dz, dz, dz};

  for (PetscInt e = 0; e < nel; e++) {
    //==================================
    // Loop over element nodes
    for (PetscInt j = 0; j < nen; j++) {
      // Get local dofs
      for (PetscInt k = 0; k < 3; k++) {
        edof[j * 3 + k] = 3 * necon[e * nen + j] + k;
      }
    }
    //==================================

    //=======================================================================
    // Loop over quadrature points to comptue average stress

    // Compute the sum of all the quadrature points
    double quadSum = 0;
    for (int iQuad = 0; iQuad < nQuad; iQuad++) {
      quadSum += momQuadPointer[iQuad][e];
    }

    double vmsSquaredElemAvg = 0.0;
    if (quadSum > quadSumMin && ignoreElemStress_p[e] == 0.0) {
      for (int iQuad = 0; iQuad < nQuad; iQuad++) {
        // get the matrices at the guass point
        std::vector<std::vector<double>> BMatrixLocal = BMatrixVector[iQuad];

        // get the stress tensor
        PetscScalar sigma[6];
        for (PetscInt j = 0; j < 6; j++) {
          sigma[j] = 0.0;
        }
        // Compute stress as C*B*u
        for (PetscInt j = 0; j < 6; j++) {
          sigma[j] = 0.0;
          for (PetscInt k = 0; k < 6; k++) {
            for (PetscInt l = 0; l < 24; l++) {
              sigma[j] += Emax * CPrivate[j][k] * BMatrixLocal[k][l] * up[edof[l]];
            }
          }
        }
        // compute the square of von mises stress at this point
        double vmSquaredAtPt
            = std::pow (sigma[0] - sigma[1], 2.0) + std::pow (sigma[1] - sigma[2], 2.0)
              + std::pow (sigma[2] - sigma[0], 2.0)
              + (6.0 * (std::pow (sigma[3], 2.0)) + 6.0 * (std::pow (sigma[4], 2.0))
                 + 6.0 * (std::pow (sigma[5], 2.0)));

        vmsSquaredElemAvg += momQuadPointer[iQuad][e] * vmSquaredAtPt / quadSum;
      }  // iQuad
    }    // if quadSum > quadSumMin

    sumsigma += PetscPowScalar (vmsSquaredElemAvg, halfpNorm);
    //=======================================================================
    // Compute Adjoint Force
    if (quadSum > quadSumMin && ignoreElemStress_p[e] == 0.0) {
      for (int iQuad = 0; iQuad < nQuad; iQuad++) {
        // get the matrices at the guass point
        std::vector<std::vector<double>> BMatrixLocal = BMatrixVector[iQuad];
        std::vector<std::vector<double>> VCBLocal = VCBVector[iQuad];

        // get the stress tensor
        PetscScalar sigma[6];
        for (PetscInt j = 0; j < 6; j++) {
          sigma[j] = 0.0;
        }

        // Compute stress as C*B*u
        for (PetscInt j = 0; j < 6; j++) {
          sigma[j] = 0.0;
          for (PetscInt k = 0; k < 6; k++) {
            for (PetscInt l = 0; l < 24; l++) {
              sigma[j] += Emax * CPrivate[j][k] * BMatrixLocal[k][l] * up[edof[l]];
            }
          }
        }

        // Compute SVCB of the element
        PetscScalar SVCB[24];  // For sigma'*V*C*B
        for (PetscInt j = 0; j < 24; j++) {
          SVCB[j] = 0.0;
          for (PetscInt k = 0; k < 6; k++) {
            SVCB[j] += Emax * VCBLocal[k][j] * sigma[k];
          }
        }

        // Compute contribution to adRHS
        PetscScalar adRHSe[24];
        for (PetscInt j = 0; j < 24; j++) {
          adRHSe[j] = pnorm * PetscPowScalar (vmsSquaredElemAvg, halfpNorm - 1.0) * SVCB[j]
                      * (momQuadPointer[iQuad][e] / quadSum);
        }

        // Assemble into adRHS vector
        ierr = VecSetValuesLocal (adRHS, 24, edof, adRHSe, ADD_VALUES);
        CHKERRQ (ierr);  // bookmark
      }                  // iQuad
    }                    // if quadsum > quadSumMin
                         //=======================================================================
  }                      // e for all elements

  // Sum up and compute approximate stress value
  PetscScalar tmp = sumsigma;
  sumsigma = 0.0;
  MPI_Allreduce (&tmp, &(sumsigma), 1, MPIU_SCALAR, MPI_SUM, PETSC_COMM_WORLD);
  maxsigmaPN = PetscPowScalar (sumsigma, 1.0 / pnorm);
  // Print approx and true max stress
  PetscPrintf (PETSC_COMM_WORLD, "Appr max stress is %f \n", maxsigmaPN);

  MPI_Barrier (PETSC_COMM_WORLD);

  // Set max p-norm stress to objective
  fx[0] = maxsigmaPN;

  // Assemble and scale adjoint load
  VecAssemblyBegin (adRHS);
  VecAssemblyEnd (adRHS);

  // Solve adjoint for this loadcase/stress constraint
  double t1, t2;
  t1 = MPI_Wtime ();
  PetscInt niter;
  PetscScalar rnorm;
  PetscScalar rnorm_adRHS;
  // Zero out possible loads in the adRHS that coincide with Dirichlet conditions
  VecPointwiseMult (adRHS, adRHS, N);
  // Solve
  ierr = KSPSolve (ksp, adRHS, adU);
  CHKERRQ (ierr);
  // Get iteration number and residual from KSP
  KSPGetIterationNumber (ksp, &niter);
  KSPGetResidualNorm (ksp, &rnorm);
  VecNorm (adRHS, NORM_2, &rnorm_adRHS);
  t2 = MPI_Wtime ();
  PetscPrintf (PETSC_COMM_WORLD,
               "Adjoint (stress) solver, BC %i LC %i SC %i:  iter: %i, rerr.: %e, time: %f\n", 0,
               0 + 1, 0 + 1, niter, rnorm / rnorm_adRHS, t2 - t1);

  // Get pointer to adjoint solution
  VecSet (LAMloc, 0.0);
  DMGlobalToLocalBegin (da_nodal, adU, INSERT_VALUES, LAMloc);
  DMGlobalToLocalEnd (da_nodal, adU, INSERT_VALUES, LAMloc);
  VecGetArray (LAMloc, &lamp);

  // Compute local contributions to sensitivity and sum with dsigdxdil
  for (PetscInt e = 0; e < nel; e++) {
    // Loop over element nodes
    for (PetscInt j = 0; j < nen; j++) {
      // Get local dofs
      for (PetscInt k = 0; k < 3; k++) {
        edof[j * 3 + k] = 3 * necon[e * nen + j] + k;
      }
    }

    double quadSum = 0;
    for (int iQuad = 0; iQuad < nQuad; iQuad++) {
      quadSum += momQuadPointer[iQuad][e] / 8.0;
    }

    PetscScalar dens = Emin + PetscPowScalar (xp[e], penal) * (Emax - Emin);
    // get the element stiffness matrix
    if (xp[e] == 1.0 || xp[e] == 0.0) {
      for (PetscInt k = 0; k < 24 * 24; k++) {
        ke[k] = KE[k] * dens;
      }
    } else {
      std::vector<double> elemQuadrature (27, 0.0);
      for (int quadNumber = 0; quadNumber < 27; quadNumber++) {
        elemQuadrature[quadNumber] = momQuadPointer[quadNumber][e];
      }
      std::vector<double> keMom = Hex8Isoparametric (X, Y, Z, 0.3, false, elemQuadrature);

      for (PetscInt k = 0; k < 24 * 24; k++) {
        // ke[k]=KE[k]*dens;
        // ke[k]=KE[k]*Emin + Emax*( keMom[k] );
        ke[k] = KE[k] * Emin + Emax * (keMom[k]) / (quadSum);
      }
    }

    PetscScalar lamKu = 0.0;
    PetscScalar uKu = 0.0;

    for (PetscInt k = 0; k < 24; k++) {
      for (PetscInt h = 0; h < 24; h++) {
        // lamKu += up[edof[k]]*KE[k*24+h]*lamp[edof[h]];
        lamKu += lamp[edof[k]] * ke[k * 24 + h] * up[edof[h]];
        uKu += up[edof[k]] * ke[k * 24 + h] * up[edof[h]];
      }
    }

    // df3[e] = -lamKu/(quadSumMin + (1.0 - quadSumMin)*quadSum/8.0);
    df3[e] = -lamKu;
  }

  // Restore everything
  VecRestoreArray (Uloc, &up);
  VecDestroy (&Uloc);
  VecRestoreArray (LAMloc, &lamp);
  VecDestroy (&LAMloc);

  VecRestoreArray (dfdx, &df3);
  VecRestoreArray (xPhys, &xp);
  VecRestoreArray (ignoreElemStress, &ignoreElemStress_p);

  // Pointer to quadrature
  for (int i = 0; i < 27; i++) {
    VecRestoreArray (momQuad[i], &momQuadPointer[i]);
  }

  return (ierr);
}


// Added by Sandy for Stress constraints; borrowed the code from Oded;
// modified for LS method by Sandy
PetscErrorCode LinearElasticity ::ComputeStress (Vec xPhys, Vec ignoreElemStress, Vec elemMaxStress,
                                                 PetscScalar Emin, PetscScalar Emax,
                                                 PetscScalar penal) {
  // Error code
  PetscErrorCode ierr;

  // Solve state eqs
  ierr = SolveState (xPhys, Emin, Emax, penal);
  CHKERRQ (ierr);

  // Get the FE mesh structure (from the nodal mesh)
  PetscInt nel, nen;
  const PetscInt *necon;
  ierr = DMDAGetElements_3D (da_nodal, &nel, &nen, &necon);
  CHKERRQ (ierr);

  // Get pointer to the densities
  PetscScalar *xp;
  VecGetArray (xPhys, &xp);

  // Get Solution
  Vec Uloc;
  DMCreateLocalVector (da_nodal, &Uloc);
  DMGlobalToLocalBegin (da_nodal, U, INSERT_VALUES, Uloc);
  DMGlobalToLocalEnd (da_nodal, U, INSERT_VALUES, Uloc);

  // get pointer to local vector
  PetscScalar *up;
  VecGetArray (Uloc, &up);

  VecSet (elemMaxStress, 0.0);
  PetscScalar *df3;
  VecGetArray (elemMaxStress, &df3);

  PetscScalar *ignoreElemStress_p;
  VecGetArray (ignoreElemStress, &ignoreElemStress_p);

  // edof array
  PetscInt edof[24];
  double vms2, vms, vm;
  // Loop over gauss points
  for (PetscInt gp = 0; gp < 8; gp++) {
    // Loop over elements
    for (PetscInt e = 0; e < nel; e++) {
      std::vector<std::vector<double>> BMatrixLocal = BMatrixVector[gp];
      // Loop over element nodes
      for (PetscInt j = 0; j < nen; j++) {
        // Get local dofs
        for (PetscInt k = 0; k < 3; k++) {
          edof[j * 3 + k] = 3 * necon[e * nen + j] + k;
        }
      }

      PetscScalar sigma[6];
      for (PetscInt j = 0; j < 6; j++) sigma[j] = 0.0;

      // Compute stress as C*B*u for E=1
      for (PetscInt j = 0; j < 6; j++) {
        sigma[j] = 0.0;
        for (PetscInt k = 0; k < 6; k++) {
          for (PetscInt l = 0; l < 24; l++) {
            sigma[j] += CPrivate[j][k] * BMatrixLocal[k][l] * up[edof[l]];
          }
        }
      }

      // Compute von Mises stress for E=1
      // First compute sigma_{VM}^2
      vms2 = std::pow (sigma[0] - sigma[1], 2.0) + std::pow (sigma[1] - sigma[2], 2.0)
             + std::pow (sigma[2] - sigma[0], 2.0)
             + (6.0 * (std::pow (sigma[3], 2.0)) + 6.0 * (std::pow (sigma[4], 2.0))
                + 6.0 * (std::pow (sigma[5], 2.0)));
      vms2 = vms2 * 0.5;
      // Then take the square root, this gives VMS stress for E=1
      vms = PetscPowScalar (vms2, 0.5);
      vm = (PetscPowScalar (xp[e], 0.5) * (Emax)) * vms;

      // only the elements that are not ignored
      if (ignoreElemStress_p[e] == 0.0) {  // do nothing
      } else
        vm = 0.0;
      df3[e] = std::max (df3[e], vm);
    }  // end of elements
  }    // end of gauss points

  // Restore everything
  VecRestoreArray (Uloc, &up);
  VecDestroy (&Uloc);

  VecRestoreArray (elemMaxStress, &df3);
  VecRestoreArray (xPhys, &xp);
  VecRestoreArray (ignoreElemStress, &ignoreElemStress_p);

  return (ierr);
}

}  // namespace para_fea