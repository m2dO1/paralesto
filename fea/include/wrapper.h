//
// Copyright 2020 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef WRAPPER_H
#define WRAPPER_H

#include <mpi.h>
#include <petsc.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include "topopt.h"
#include "topopt1d.h"

/*! \file wrapper.h
    \brief A file that constains classes for interfacing with PETSc

    Interfaces how finite element data is stored by PETSc and how it's
    stored/ordered within the level set grid.
*/

namespace para_fea {

/*! \class Wrapper
    \brief Provides a map between how elements are stored in PETSc framework (ie
    distributed among processors) vs ordered level set grid storage.
*/
class Wrapper {
 public:
  //! Determine mapping
  /*! \param opt
        Optimization object for linear elasticity

      \param volfrac_to_petsc_map
        Mapping between level set volume fraction storage to PETSc volume
        fraction storage

      \param size_of_elements
        Number of elements in the mesh. Note that the number of elements in
        the level set grid must be the same as the number of elements in the
        PETSc mesh.
  */
  void GetMap (TopOpt *opt, std::vector<int> &volfrac_to_petsc_map, int size_of_elements);

  //! Determine mapping
  /*! \param opt
        Optimization object for heat conduction

      \param volfrac_to_petsc_map
        Mapping between level set volume fraction storage to PETSc volume
        fraction storage

      \param size_of_elements
        Number of elements in the mesh. Note that the number of elements in
        the level set grid must be the same as the number of elements in the
        PETSc mesh.
  */
  void GetMap (TopOpt1D *opt, std::vector<int> &volfrac_to_petsc_map, int size_of_elements);
};


/*! \class MyGather
    \brief Class that gathers necessary information from distributed processors
*/
class MyGather {
 public:
  //! Constructor
  /*! \param vin
   */
  MyGather (Vec vin);

  //! Deconstructor
  ~MyGather ();

  int temp;
  double *sensiArray;
  Vec vout;
};


//! Contains functions for mapping volume fraction between the level set
//! storage to PETSc storage
namespace petsc_map {
//! Provides a map between level set grid and PETSc framework
/*! Map is typically between how volume fractions are stored/ordered in the
    level set vs how they are stored in PETSc framework (ie distributed among
    processors), but can be used for any general quantity, e.g. elements, nodes,
    etc.

    \param da
      Distributed array for the data of the structured mesh. (Typically element
      data, but nodal can be used as well.)

    \param lsm_to_petsc_map
      Mapping between level set ordering to PETSc ordering

    \param vec_size
      Size of the lsm_to_petsc_map vector. Typically the number of elements in
      the grid. Note that the number of elements in the level set grid must be
      the same as the number of elements in the PETSc mesh. Also note that the
      size can also be the number of nodes in the mesh.
*/
void GetMap (DM da, std::vector<int> &lsm_to_petsc_map, int vec_size);
}  // namespace petsc_map

}  // namespace para_fea

#endif