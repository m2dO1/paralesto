//
// Copyright 2021 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef PHYSICSWRAPPER_H
#define PHYSICSWRAPPER_H

#include <memory>
#include <string>
#include <vector>

#include "initialize.h"
#include "linear_elasticity.h"
#include "petsc.h"
#include "poisson.h"
#include "topopt.h"
#include "topopt1d.h"
#include "wrapper.h"

/*! \file physics_wrapper.h
    \brief A file that contains the class for wrapping the finite element
    analysis of the topology optimization framework into a single object.
*/

namespace paralesto {
class WrapperIO;  // forward declare wrapper class in appropriate namespace
}  // namespace paralesto

namespace para_fea {

/*! \class PhysicsWrapper
    \brief Wrapper for all the physics modeling (finite element analysis and
    sensitivity calculation) of the topology optimization framework into a
    single object
*/
class PhysicsWrapper {
  friend class paralesto::WrapperIO;

 public:
  using TopOptPtr = std::unique_ptr<TopOpt>;
  using LinElasticPtr = std::unique_ptr<LinearElasticity>;
  using TopOpt1DPtr = std::unique_ptr<TopOpt1D>;
  using PoissonPtr = std::unique_ptr<Poisson>;
  using WrapperPtr = std::unique_ptr<Wrapper>;

  //! Constructor
  /*! \param phys_init
        Initialization object for the physics (FEA) input file.
  */
  PhysicsWrapper (InitializeOpt &phys_init);

  //! Destructor
  ~PhysicsWrapper ();

  //! Calculate the derivative of the objective with respect to density for a
  //! multiphysics problem.
  /*! \param densities
        Vector of element densities.

      \param is_print
        Prints out progress statements during method runtime. Useful for
        debugging.

      \return
        Vector of sensitivities (df/drho) for each element.

      \note This method currently assumes that the partials are for compliance
  */
  std::vector<double> CalculatePartialDerivatives (std::vector<double> densities,
                                                   bool is_print = false);

  //! Calculate the derivative of thermal compliance with respect to density
  //! for a poisson (heat conduction) problem.
  /*! \param densities
        Vector of element densities.

      \param is_print
        Prints out progress statements during method runtime. Useful for
        debugging.

      \return
        Vector of sensitivities (df/drho) for each element.
  */
  std::vector<double> CalculatePartialCompliance (std::vector<double> densities,
                                                  bool is_print = false);

  //! Exclude elements from stress calculations.
  /*! \param ignore_elem
        Vector of elements to exclude in stress calculation. True (1)
        indicates that the element should be excluded, False (0) indicates
        that the element should not be excluded.
  */
  std::vector<double> CalculatePartialThermalCompliance (std::vector<double> densities,
                                                         bool is_print = false);

  //! Exclude elements from stress calculations.
  /*! \param ignore_elem
        Vector of elements to exclude in stress calculation. True (1)
        indicates that the element should be excluded, False (0) indicates
        that the element should not be excluded.
  */
  void ExcludeElementsFromStress (std::vector<bool> ignore_elem);

  //! Calculate the derivative of stress with respect to density for a linear
  //! elastic problem.
  /*! \param densities
        Vector of element densities.

      \param ignore_elem
        Vector of elements to exclude in stress calculation. True (1)
        indicates that the element should be excluded, False (0) indicates
        that the element should not be excluded.

      \param is_print
        Prints out progress statements during method runtime. Useful for
        debugging.

      \return
        Vector of sensitivities (df/drho) for each element.
  */
  std::vector<double> CalculatePartialStress (std::vector<double> densities, bool is_print = false);

  //! Get the map for lsm to PETSc ordering
  std::vector<int> GetLsmPetscMap () { return (lsm_to_petsc_map); };

  void WriteDispToTxt (int count_iter, std::string file_path = "", std::string file_name = "disp");

  void WriteTempToTxt (int count_iter, std::string file_path = "", std::string file_name = "temp");

 private:
  //! Assigns volume fractions from the level set grid to a PETSc vector
  /*! This method assigns the volume fractions for a single physics, i.e. to
      the PETSc vector that belongs to a specific physics.

      \param lsm_dens
        Vector of element densities from the level set grid

      \param x_phys
        Vector of element densities for the FEA mesh
  */
  void AssignVolumeFractions (std::vector<double> lsm_dens, Vec x_phys);

  //! Assigns volume fractions from the level set grid to all physics
  /*! This method assigns the volume fractions for all physics, i.e. PETSc
      vectors for all physics (linear elasticity and poisson as of 2020-03-08)

      \param densities
        Vector of element densities
  */
  void AssignVolumeFractionsMultiPhys (std::vector<double> densities);

  //! Calculate density sensitivities (partials) for multi-physics problem
  /*! This method couples heat conduction and linear elasticity. It assumes
      two separate load cases (strucutral and thermal) and computes the
      compliance sensitivities for each load case and adds them together.

      \param elem_sens
        Vector of compliance sensitivities with respect to element density
  */
  PetscErrorCode CalculateMultiPhysicsSeparateLoads (std::vector<double> &elem_sens);

  int nelem;  //!< number of elements in the mesh
  int nnode;  //!< number of nodes in the mesh

  //! Variable for storing rank of the calling process in the communicator
  PetscMPIInt rank;

  /*! \name PETSc mapping */
  ///\{
  //! Volume fractions for PETSc mesh
  std::vector<double> petsc_volfrac = std::vector<double> (1, 0.2);

  //! Booleans for ignoring elements for stress calculation
  /*! True (1) indicates that the element should be excluded, False (0)
      indicates that the element should not be excluded.
  */
  std::vector<bool> ignore_stress = std::vector<bool> (1, 0);

  //! Map between elements in the level set grid and PETSc mesh
  std::vector<int> lsm_to_petsc_map = std::vector<int> (1, 0);
  ///\}

  /*! \name Pointers to finite element analysis objects */
  ///\{
  TopOptPtr opt_ptr;            //!< Pointer to (linear elastic) optimization object
  LinElasticPtr phys_lin_ptr;   //!< Pointer to linear elastic physics object
  TopOpt1DPtr opt_poisson_ptr;  //!< Pointer to (poisson) optimization object
  PoissonPtr phys_poisson_ptr;  //!< Pointer to poisson physics object
  WrapperPtr wrapper_ptr;       //!< Pointer to PETSc wrapper object
                                ///\}
};

/*! \class LinElasticWrapper
    \brief Wrapper for the physics modeling (finite element analysis) of linear
    elasticity in the topology optimization framework
*/
class LinElasticWrapper {
  friend class paralesto::WrapperIO;

 public:
  using TopOptPtr = std::unique_ptr<TopOpt>;
  using LinElasticPtr = std::unique_ptr<LinearElasticity>;
  using WrapperPtr = std::unique_ptr<Wrapper>;

  //! Constructor
  /*! \param phys_init
        Initialization object for the physics (FEA) input file.
  */
  LinElasticWrapper (InitializeOpt &phys_init);

  //! Destructor
  ~LinElasticWrapper ();

  //! Solves the linear elasticity state equation
  /*! \param densities
        Vector of element densities.

      \param is_print
        Prints out progress statements during method runtime. Useful for
        debugging.
  */
  void SolveState (std::vector<double> densities, bool is_print = false, double rel_tol = 1e-5);

  // TODO: delete after testing
  MyGather CalculatePartialsTest (bool is_print = false);

  //! Get pointer to the FE solution of the state equation
  Vec GetStateField ();

  //! Get pointer to the FE right-hand side (RHS) of the state equation
  Vec GetStateRhs ();

  // TODO: delete after testing
  std::vector<int> GetLsmPetscMap () { return (lsm_to_petsc_map); };

  void WriteDispToTxt (int count_iter, std::string file_path = "", std::string file_name = "disp");

 private:
  //! Assigns volume fractions to PETSc mesh from provided vector
  /*! \param densities
        Vector of element densities
  */
  void AssignVolumeFractions (std::vector<double> densities);

  int nelem;  //!< number of elements in the mesh
  int nnode;  //!< number of nodes in the mesh

  //! Variable for storing rank of the calling process in the communicator
  PetscMPIInt rank;

  /*! \name PETSc volume fraction mapping */
  ///\{
  //! Volume fractions for PETSc mesh
  std::vector<double> petsc_volfrac = std::vector<double> (1, 0.2);

  //! Map between volume fractions on the level set grid and PETSc mesh
  std::vector<int> lsm_to_petsc_map = std::vector<int> (1, 0);
  ///\}

  /*! \name Pointers to helper objects for the finite element analysis */
  ///\{
  TopOptPtr opt_ptr;       //!< Pointer to data and mesh container
  LinElasticPtr phys_ptr;  //!< Pointer to linear elastic physics object
  WrapperPtr wrapper_ptr;  //!< Pointer to PETSc wrapper object
                           ///\}
};

/*! \class PoissonWrapper
    \brief Wrapper for the physics modeling (finite element analysis) of poisson
    problem in the topology optimization framework
*/
class PoissonWrapper {
  friend class paralesto::WrapperIO;

 public:
  using TopOpt1DPtr = std::unique_ptr<TopOpt1D>;
  using PoissonPtr = std::unique_ptr<Poisson>;
  using WrapperPtr = std::unique_ptr<Wrapper>;

  //! Constructor
  /*! \param phys_init
        Initialization object for the physics (FEA) input file.
  */
  PoissonWrapper (InitializeOpt &phys_init);

  //! Destructor
  ~PoissonWrapper ();

  //! Solves the poisson state equation
  /*! \param densities
        Vector of element densities.

      \param is_print
        Prints out progress statements during method runtime. Useful for
        debugging.
  */
  void SolveState (std::vector<double> densities, bool is_print = false, double rel_tol = 1e-5);

  // TODO: delete after testing
  MyGather CalculatePartialsTest (bool is_print = false);

  //! Get pointer to the FE solution of the state equation
  Vec GetStateField ();

  //! Get pointer to the FE right-hand side (RHS) of the state equation
  Vec GetStateRhs ();

  // TODO: delete after testing
  std::vector<int> GetLsmPetscMap () { return (lsm_to_petsc_map); };

  void WriteTempToTxt (int count_iter, std::string file_path = "", std::string file_name = "temp");

 private:
  //! Assigns volume fractions to PETSc mesh from provided vector
  /*! \param densities
        Vector of element densities
  */
  void AssignVolumeFractions (std::vector<double> densities);

  int nelem;  //!< number of elements in the mesh
  int nnode;  //!< number of nodes in the mesh

  //! Variable for storing rank of the calling process in the communicator
  PetscMPIInt rank;

  /*! \name PETSc volume fraction mapping */
  ///\{
  //! Volume fractions for PETSc mesh
  std::vector<double> petsc_volfrac = std::vector<double> (1, 0.2);

  //! Map between volume fractions on the level set grid and PETSc mesh
  std::vector<int> lsm_to_petsc_map = std::vector<int> (1, 0);
  ///\}

  /*! \name Pointers to helper objects for the finite element analysis */
  ///\{
  TopOpt1DPtr opt_ptr;     //!< Pointer to data and mesh container
  PoissonPtr phys_ptr;     //!< Pointer to linear elastic physics object
  WrapperPtr wrapper_ptr;  //!< Pointer to PETSc wrapper object
                           ///\}
};

}  // namespace para_fea

#endif