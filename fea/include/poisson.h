/*
 Authors: Niels Aage, Erik Andreassen, Boyan Lazarov, August 2013

 Disclaimer:
 The authors reserves all rights but does not guaranty that the code is
 free from errors. Furthermore, we shall not be liable in any event
 caused by the use of the program.
*/

#ifndef POISSON_H
#define POISSON_H

#include <math.h>
#include <petsc.h>
#include <petsc/private/dmdaimpl.h>

#include <iostream>
#include <vector>

#include "initialize.h"
#include "topopt1d.h"

/*! \file poisson.h
    \brief A file that has the class for poisson problem physics
*/

namespace para_fea {

/*! \class Poisson
    \brief Defines poisson problem physics
*/
class Poisson {
 public:
  //! Constructor
  /*! \param da_nodes
        Nodal mesh

      \param myInit
        Initialization object for the input file
  */
  Poisson (DM da_nodes, InitializeOpt &myInit);

  //! Destructor
  ~Poisson ();

  //! Compute objective and constraints and sensitivities at once:
  /*! GOOD FOR SELF_ADJOINT PROBLEMS
      \param fx
        Objective function (compliance)

      \param dfdx
        Sensitivity of the objective function (compliance) to element density

      \param xPhys
        Element densities

      \param Kmin
        Thermal conductivity coefficient for void material. Void material has
        a thermal conductivity coefficient of 0.0, but to avoid
        ill-conditioning a very small number (relative to the thermal
        conductivity coefficient of the solid material) should be used.

      \param Kmax
        Thermal conductivity coefficient of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode ComputeSensitivities (PetscScalar *fx, Vec dfdx, Vec xPhys, PetscScalar Kmin,
                                       PetscScalar Kmax, PetscScalar penal = 1.0);

  // TODO: delete method after debugging
  PetscErrorCode ComputeSensitivitiesTest (PetscScalar *fx, Vec dfdx, Vec xPhys, PetscScalar Kmin,
                                           PetscScalar Kmax, PetscScalar penal = 1.0);

  //! \todo TODO(Carolina): add description
  /*! \param xPhys
        Element densities

      \param DeltaTVec
        Temperature change at element centroid

      \param Kmin
        Thermal conductivity coefficient for void material. Void material has
        a thermal conductivity coefficient of 0.0, but to avoid
        ill-conditioning a very small number (relative to the thermal
        conductivity coefficient of the solid material) should be used.

      \param Kmax
        Thermal conductivity coefficient of the (solid) material.

      \param withConvection
        Whether heat problem uses convection. False by default. \todo
        TODO(Carolina): check this description

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode ComputeCentroidTemperatures (Vec xPhys, Vec DeltaTVec, PetscScalar Kmin,
                                              PetscScalar Kmax, bool withConvection = false,
                                              PetscScalar penal = 1.0);

  //! Compute sensitivites for the adjoint solution
  /*! \param dfdx
        Sensitivity of the adjoint to element density

      \param xPhys
        Element densities

      \param DeltaTVecAdj
        Temperature adjoint change at element centroid.

      \param Kmin
        Thermal conductivity coefficient for void material. Void material has
        a thermal conductivity coefficient of 0.0, but to avoid
        ill-conditioning a very small number (relative to the thermal
        conductivity coefficient of the solid material) should be used.

      \param Kmax
        Thermal conductivity coefficient of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode ComputeAdjointSensitivities (Vec dfdx, Vec xPhys, Vec DeltaTVecAdj,
                                              PetscScalar Kmin, PetscScalar Kmax,
                                              PetscScalar penal = 1.0);

  //! Compute the adjoint load
  /*! \param DeltaTVecAdj
        Temperature adjoint change at element centroid.
  */
  PetscErrorCode ComputeAdjointLoadFromCentroids (Vec DeltaTVecAdj);

  //! \todo TODO(Caroina): add description
  /*! \param fx
        Objective function.

      \param dfdx
        Sensitivity of the adjoint to element density.

      \param xPhys
        Element densities.

      \param Kmin
        Thermal conductivity coefficient for void material. Void material has
        a thermal conductivity coefficient of 0.0, but to avoid
        ill-conditioning a very small number (relative to the thermal
        conductivity coefficient of the solid material) should be used.

      \param Kmax
        Thermal conductivity coefficient of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation.
  */
  PetscErrorCode ComputeSurfaceConvectionSensitivities (PetscScalar *fx, Vec dfdx, Vec xPhys,
                                                        PetscScalar Kmin, PetscScalar Kmax,
                                                        PetscScalar penal = 1.0);

  //! Solve the FE problem
  /*! \param xPhys
        Element densities

      \param DeltaTVec
        Elemental temperature

      \param Kmin
        Thermal conductivity coefficient for void material. Void material has
        a thermal conductivity coefficient of 0.0, but to avoid
        ill-conditioning a very small number (relative to the thermal
        conductivity coefficient of the solid material) should be used.

      \param Kmax
        Thermal conductivity coefficient of the (solid) material.

      \param withConvection
        Whether heat problem uses convection. False by default. \todo
        TODO(Carolina): check this description

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode SolveState (Vec xPhys, PetscScalar Kmin, PetscScalar Kmax,
                             bool withConvection = false, PetscScalar penal = 1.0);

  //! Get pointer to the FE solution
  Vec GetStateField () { return (U); };

  //! Get pointer to the FE solution
  Vec GetStateRHS () { return (RHS); };

  //! Get pointer to DMDA
  DM GetDM () { return (da_nodal); };

  DM da_nodal;  //!< Logical mesh (nodal)

  /*! \name For stress and other adjoint related objective functions*/
  ///\{
  Vec adU;    //!< adjoint temp vector
  Vec adRHS;  //!< adjoint load vector
  ///\}

  PetscScalar myRTOL = 1.0e-9;

 private:
  // Logical mesh
  PetscInt nn[3];     // Number of nodes in each direction
  PetscInt ne[3];     // Number of elements in each direction
  PetscScalar xc[6];  // Domain coordinates

  /*! \name For solving [K][U] = [RHS]*/
  ///\{
  PetscScalar KE[8 * 8];  //!< Element stiffness matrix
  Mat K;                  //!< Global stiffness matrix
  Vec U;                  //!< Temperature vector
  Vec RHS;                //!< Load vector
  Vec N;                  //!< Dirichlet vector (used when imposing BCs)
  KSP ksp;                //!< Pointer to the KSP object i.e. the linear solver+preconditioner
  PetscInt nlvls;         //!< number of levels for multi-grid solver/preconditioner
  Vec HeatLoad;           //!< heat load vector
  Vec Tgiven;             //!< heat load vector
  ///\}

  //! Set up the FE mesh and data structures
  /*! \param da_nodes
        Nodal mesh

      \param myInit
        Initialization object for the input file
  */
  PetscErrorCode SetUpLoadAndBC (DM da_nodes, InitializeOpt &myInit);

  //! Solve adjoint FE problem
  /*! \param xPhys
        Element densities

      \param Kmin
        Thermal conductivity coefficient for void material. Void material has
        a thermal conductivity coefficient of 0.0, but to avoid
        ill-conditioning a very small number (relative to the thermal
        conductivity coefficient of the solid material) should be used.

      \param Kmax
        Thermal conductivity coefficient of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode SolveAdjointState (Vec xPhys, PetscScalar Kmin, PetscScalar Kmax,
                                    PetscScalar penal = 1.0);

  //! Assemble the stiffness matrix
  /*! \param xPhys
        Element densities

      \param Kmin
        Thermal conductivity coefficient for void material. Void material has
        a thermal conductivity coefficient of 0.0, but to avoid
        ill-conditioning a very small number (relative to the thermal
        conductivity coefficient of the solid material) should be used.

      \param Kmax
        Thermal conductivity coefficient of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode AssembleStiffnessMatrix (Vec xPhys, PetscScalar Kmin, PetscScalar Kmax,
                                          PetscScalar penal = 1.0);

  //! Assemble surface convection matrices
  /*! Assembles stiffness matrix and forces considering surface convection

      \param xPhys
        Element densities.

      \param DeltaTVecFlux

      \param areaToVolume
        Helps to convert area based surface convection to a volume based load.

      \param Kmin
        Thermal conductivity coefficient for void material. Void material has
        a thermal conductivity coefficient of 0.0, but to avoid
        ill-conditioning a very small number (relative to the thermal
        conductivity coefficient of the solid material) should be used.

      \param Kmax
        Thermal conductivity coefficient of the (solid) material.

      \param hSurfConv
        Surface convection.

      \param Tambi
        Ambient temperature? \todo TODO(Carolina): check description

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode AssembleSurfaceConvection (Vec xPhys, Vec DeltaTVecFlux,
                                            std::vector<Vec> areaToVolume, PetscScalar Kmin,
                                            PetscScalar Kmax, double hSurfConv, double Tambi,
                                            PetscScalar penal = 1.0);

  //! Set up the solver
  PetscErrorCode SetUpSolver ();

  //! Routine that doesn't change the element type upon repeated calls
  /*! \param dm

      \param nel

      \param nen

      \param e
  */
  PetscErrorCode DMDAGetElements_3D (DM dm, PetscInt *nel, PetscInt *nen, const PetscInt *e[]);

  // Methods used to assemble the element stiffness matrix
  /*! \param X

      \param Y

      \param Z

      \param nu

      \param redInt

      \param ke
  */
  PetscInt Hex8Isoparametric (PetscScalar *X, PetscScalar *Y, PetscScalar *Z, PetscScalar nu,
                              PetscInt redInt, PetscScalar *ke);

  //! \todo TODO(Carolina): add description
  /*! \param v1
      \param v2
      \param l
  */
  PetscScalar Dot (PetscScalar *v1, PetscScalar *v2, PetscInt l);

  //! \todo TODO(Carolina): add description
  /*! \param xi

      \param eta

      \param zeta

      \param dNdxi

      \param dNdeta

      \param dNdzeta
  */
  void DifferentiatedShapeFunctions (PetscScalar xi, PetscScalar eta, PetscScalar zeta,
                                     PetscScalar *dNdxi, PetscScalar *dNdeta, PetscScalar *dNdzeta);

  //! \todo TODO(Carolina): add description
  /*! \param J

      \param invJ
  */
  PetscScalar Inverse3M (PetscScalar J[][3], PetscScalar invJ[][3]);

  //! Bmatrix at centroid
  /*! \note Small enough to be allocated on stack
   */
  PetscScalar Bmatrix[3][8];

  /*! \name Auxiliary variables*/
  ///\{
  PetscInt dim = 1;       //!< dimensionality for degrees of freedom per node
  PetscInt spacedim = 3;  //!< dimensionality of the problem, i.e. 3D
  double areaScaleFactor;
  ///\}
};

}  // namespace para_fea

#endif