/*
 Authors: Niels Aage, Erik Andreassen, Boyan Lazarov, August 2013
 Updated: June 2019, Niels Aage
 Copyright (C) 2013-2019,

 Disclaimer:
 The authors reserves all rights but does not guaranty that the code is
 free from errors. Furthermore, we shall not be liable in any event
 caused by the use of the program.
*/

#ifndef LINEAR_ELASTICITY_H
#define LINEAR_ELASTICITY_H

#include <math.h>
#include <petsc.h>
#include <petsc/private/dmdaimpl.h>

#include <iostream>
#include <vector>

#include "initialize.h"
#include "topopt.h"

/*! \file linear_elasticity.h
    \brief A file that has the class linear elastic physics
*/

namespace para_fea {

/*! \class LinearElasticity
    \brief Defines linear elastic physics
*/
class LinearElasticity {
 public:
  //! Constructor
  /*! \param da_nodes
        Nodal mesh

      \param myInit
        Initialization object for the input file
  */
  LinearElasticity (DM da_nodes, InitializeOpt &myInit);

  //! Destructor
  ~LinearElasticity ();

  //! Compute objective and constraints and sensitivities at once
  /*! GOOD FOR SELF_ADJOINT PROBLEMS
      \param fx
        Objective function (compliance)

      \param dfdx
        Sensitivity of the objective function (compliance) to element density

      \param xPhys
        Element densities.

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation.
  */
  PetscErrorCode ComputeSensitivities (PetscScalar *fx, Vec dfdx, Vec xPhys, PetscScalar Emin,
                                       PetscScalar Emax, PetscScalar penal = 1.0);

  // TODO: delete method after debugging
  PetscErrorCode ComputeSensitivitiesTest (PetscScalar *fx, Vec dfdx, Vec xPhys, PetscScalar Emin,
                                           PetscScalar Emax, PetscScalar penal = 1.0);

  //! Compute stress
  /*! \param xPhys
        Element densities.

      \param ignoreElemStress
        Vector of 0's or 1's that indicate whether to ignore the stress of an
        element. Value of 0 indicates that element should be ignored.

      \param elemMaxStress
        Vector for assigning the maximum stress of each element.

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation.
  */
  PetscErrorCode ComputeStress (Vec xPhys, Vec ignoreElemStress, Vec elemMaxStress,
                                PetscScalar Emin, PetscScalar Emax, PetscScalar penal = 1.0);

  //! Stress sensitivities
  /*! \param fx
        Objective function (stress)

      \param dfdx
        Sensitivity of the objective function (stress) to element density

      \param xPhys
        Element densities.

      \param ignoreElemStress
        Vector of 0's or 1's that indicate whether to ignore the stress of an
        element. Value of 1 indicates that element should be ignored.

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation.
  */
  PetscErrorCode ComputeStressSensitivitiesSandy (PetscScalar *fx, Vec dfdx, Vec xPhys,
                                                  Vec ignoreElemStress, PetscScalar Emin,
                                                  PetscScalar Emax, PetscScalar penal = 1.0);

  //! Stress sensitivities from custom quadrature
  /*! \param fx
        Objective function (stress)

      \param dfdx
        Sensitivity of the objective function (stress) to element density

      \param xPhys
        Element densities.

      \param ignoreElemStress
        Vector of 0's or 1's that indicate whether to ignore the stress of an
        element. Value of 1 indicates that element should be ignored.

      \param momQuad
        \todo TODO(Carolina): add description

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation.
  */
  PetscErrorCode ComputeStressSensitivitiesFromQuadrature (PetscScalar *fx, Vec dfdx, Vec xPhys,
                                                           Vec ignoreElemStress,
                                                           std::vector<Vec> momQuad,
                                                           PetscScalar Emin, PetscScalar Emax,
                                                           PetscScalar penal = 1.0);

  //! Thermal rotation sensitivities
  /*! \param fx
        Objective function (compliance)

      \param dfdx
        Sensitivity of the objective function (compliance) to element density

      \param xPhys
        Element densities.

      \param DeltaTVec
        Temperature change at element centroid.

      \param DeltaTVecAdj
        Temperature adjoint change at element centroid.

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param alphaT
        Coefficient of thermal expansion.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation.
  */
  PetscErrorCode ComputeThRotSensitivities (PetscScalar *fx, Vec dfdx, Vec xPhys, Vec DeltaTVec,
                                            Vec DeltaTVecAdj, PetscScalar Emin, PetscScalar Emax,
                                            PetscScalar alpha_t, PetscScalar penal = 1.0);

  //! Solve the FE problem
  /*! \param xPhys
        Element densities

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode SolveState (Vec xPhys, PetscScalar Emin, PetscScalar Emax,
                             PetscScalar penal = 1.0);

  //! Solve the FE problem from custom quadrature
  /*! \param xPhys
        Element densities

      \param momQuad
        \todo TODO(Carolina): add description

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode SolveState (Vec xPhys, std::vector<Vec> momQuad, PetscScalar Emin,
                             PetscScalar Emax, PetscScalar penal = 1.0);

  //! Solve the thermo-elastic FE problem
  /*! \param xPhys
        Element densities

      \param delta_t
        Temperature change at element centroid

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param alphaT
        Coefficient of thermal expansion.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode SolveState (Vec xPhys, Vec delta_t, PetscScalar Emin, PetscScalar Emax,
                             PetscScalar alpha_t, PetscScalar penal = 1.0);

  //! Solve state problem if only the thermal load is applied
  /*! \param xPhys
        Element densities

      \param delta_t
        Temperature change at element centroid

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param alphaT
        Coefficient of thermal expansion.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode SolveThermalOnlyState (Vec xPhys, Vec delta_t, PetscScalar Emin, PetscScalar Emax,
                                        PetscScalar alpha_t, PetscScalar penal = 1.0);

  //! Solve adjoint FE problem
  /*! \param xPhys
        Element densities

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode SolveAdjointState (Vec xPhys, PetscScalar Emin, PetscScalar Emax,
                                    PetscScalar penal = 1.0);

  //! Compute element displacements (in a particular direction?)
  /*! \param elemDisp

      \param xPhys
        Element densities

      \param dirArr
  */
  PetscErrorCode ComputeElemDirDisp (Vec elemDisp, Vec xPhys, std::vector<double> dirArr);

  //! Compute element displacements (magnitude?)
  /*! \param elemDisp

      \param xPhys
        Element densities
  */
  PetscErrorCode ComputeElemMagDisp (Vec elemDisp, Vec xPhys);

  //! Compute objective and constraints and sensitivities using custom quadrature
  /*! \param opt
        Optimization object
  */
  PetscErrorCode ComputeSensitivitiesFromQuadrature (PetscScalar *fx, Vec dfdx, Vec xPhys,
                                                     std::vector<Vec> momQuad, PetscScalar Emin,
                                                     PetscScalar Emax, PetscScalar penal = 1.0);

  //! Get pointer to the FE solution
  Vec GetStateField () { return (U); };

  //! Get pointer to the FE solution
  Vec GetStateRHS () { return (RHS); };

  //! Get pointer to DMDA
  DM GetDM () { return (da_nodal); };

  DM da_nodal;  //!< Logical mesh (nodal)

  /*! \name For stress and other adjoint related objective functions*/
  ///\{
  Vec adU;    //!< Adjoint solution
  Vec adRHS;  //!< Adjoint load
  ///\}

  /*! \name temporary*/
  ///\{
  Vec RHS1;  //!< load vector
  Vec RHS2;  //!< load vector
  ///\}

  PetscScalar myRTOL = 1.0e-5;

 private:
  // Logical mesh
  PetscInt nn[3];     // Number of nodes in each direction
  PetscInt ne[3];     // Number of elements in each direction
  PetscScalar xc[6];  // Domain coordinates

  /*! \name For solving [K][U] = [RHS]*/
  ///\{
  PetscScalar KE[24 * 24];  //!< Element stiffness matrix
  Mat K;                    //!< Global stiffness matrix
  Vec U;                    //!< Displacement vector
  Vec RHS;                  //!< Load vector
  Vec N;                    //!< Dirichlet vector (used when imposing BCs)
  KSP ksp;                  //!< Pointer to the KSP object i.e. the linear solver+preconditioner
  PetscInt nlvls;           //!< number of levels for multi-grid solver/preconditioner
  PetscScalar nu;           //!< Possions ratio
  ///\}

  //! Set up the FE mesh and data structures
  /*! \param da_nodes
        Nodal mesh

      \param myInit
        Initialization object for the input file
  */
  PetscErrorCode SetUpLoadAndBC (DM da_nodes, InitializeOpt &myInit);

  //! Solve the thermo-elastic FE problem from custom quadrature
  /*! \param xPhys
        Element densities

      \param momQuad
        \todo TODO(Carolina): add description

      \param delta_t
        Temperature change at element centroid

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param alphaT
        Coefficient of thermal expansion.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation

      \warning Method does not work. Element thermal expansion vector needs to
      be assembled from quadrature in Hex8Isoparametric method. A new
      AssembleThermalExpansionLoad may also need to be created. Move to public
      once fixed.
  */
  PetscErrorCode SolveState (Vec xPhys, std::vector<Vec> momQuad, Vec delta_t, PetscScalar Emin,
                             PetscScalar Emax, PetscScalar alpha_t, PetscScalar penal = 1.0);

  //! Assemble the stiffness matrix
  /*! \param xPhys
        Element densities

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode AssembleStiffnessMatrix (Vec xPhys, PetscScalar Emin, PetscScalar Emax,
                                          PetscScalar penal = 1.0);

  // Assemble the stiffness matrix from quadrature
  /*! \param xPhys
        Element densities

      \param momQuad
        \todo TODO(Carolina): add description

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode AssembleStiffnessMatrixFromQuadrature (Vec xPhys, std::vector<Vec> momQuad,
                                                        PetscScalar Emin, PetscScalar Emax,
                                                        PetscScalar penal = 1.0);

  //! Assemble the thermal expansion load
  /*! \param xPhys
        Element densities

      \param delta_t
        Temperature change at element centroid

      \param Emin
        Young's modulus for void material. Void material has a Young's modulus
        of 0.0, but to avoid ill-conditioning a very small number (relative to
        the Young's modulus of the solid material) should be used.

      \param Emax
        Young's modulus of the (solid) material.

      \param alphaT
        Coefficient of thermal expansion.

      \param penal
        Penalization of the densities. Should be 1.0 for Ersatz material
        interpolation
  */
  PetscErrorCode AssembleThermalExpansionLoad (Vec xPhys, Vec delta_t, PetscScalar Emin,
                                               PetscScalar Emax, PetscScalar alphaT,
                                               PetscScalar penal);

  //! Set up the solver
  PetscErrorCode SetUpSolver ();

  //! Helper method for solving the state equation
  /*! \param t1
        Start time for the SolveState method call
  */
  PetscErrorCode SolveStateHelper (double t1);

  //! Routine that doesn't change the element type upon repeated calls
  /*! \param dm

      \param nel

      \param nen

      \param e
  */
  PetscErrorCode DMDAGetElements_3D (DM dm, PetscInt *nel, PetscInt *nen, const PetscInt *e[]);

  /*! \name Methods used to assemble the element stiffness matrix*/
  ///\{

  //! Calculates a HEX8 isoparametric element stiffness matrix
  /*! Note that due to the fact that the mesh is structured and the elements
      are linear, the element stiffness matrix is constant for all elements.
      The element stiffness matrix is computed as:
        ke = int(int(int(B^T*C*B,x),y),z)
      For an isoparameteric element this integral becomes:
        ke = int(int(int(B^T*C*B*det(J),xi=-1..1),eta=-1..1),zeta=-1..1)
      where B is the more complicated expression:
        B = [d_x*alpha1 + d_y*alpha2 + d_z*alpha3]*N
      where
        d_x = [invJ11 invJ12 invJ13]*[d_xi d_eta d_zeta]
        d_y = [invJ21 invJ22 invJ23]*[d_xi d_eta d_zeta]
        d_z = [invJ31 invJ32 invJ33]*[d_xi d_eta d_zeta]

      \note The elasticity modulus is left out of the calculation, because it
      is multiplied afterwards (the aim is topology optimization).

      \param X
        Vector containing the local x coordinates of the eight nodes (x0, x1,
        x2, x3, x4, x5, x6, x7). Where node 0 is the lower left corner, node 1
        is the next node in the counter-clockwise direction, etc. Node 0, 1,
        2, 3 occur at z = 0 and nodes 4, 5, 6, 7 are ordered in the same
        manner, but at the top of the element.

      \param Y
        Vector containing the local y coordinates of the eight nodes (y0, y1,
        y2, y3, y4, y5, y6, y7). Where node 0 is the lower left corner, node 1
        is the next node in the counter-clockwise direction, etc. Node 0, 1,
        2, 3 occur at z = 0 and nodes 4, 5, 6, 7 are ordered in the same
        manner, but at the top of the element.

      \param Z
        Vector containing the local z coordinates of the eight nodes (z0, z1,
        z2, z3, z4, z5, z6, z7). Where node 0 is the lower left corner, node 1
        is the next node in the counter-clockwise direction, etc. Node 0, 1,
        2, 3 occur at z = 0 and nodes 4, 5, 6, 7 are ordered in the same
        manner, but at the top of the element.

      \param nu
        Poisson's ratio.

      \param redInt
        Reduced integration option. If 0, use full integration. If 1, use
        reduced integration.

      \param ke
        Element stiffness matrix. \note Needs to be multiplied with elasticity
        modulus.
  */
  PetscInt Hex8Isoparametric (PetscScalar *X, PetscScalar *Y, PetscScalar *Z, PetscScalar nu,
                              PetscInt redInt, PetscScalar *ke);

  //! \todo TODO(Carolina): add description
  /*! \param X

      \param Y

      \param Z

      \param nu

      \param redInt

      \param ke
  */
  std::vector<double> Hex8Isoparametric (PetscScalar *X, PetscScalar *Y, PetscScalar *Z,
                                         PetscScalar nu, PetscInt redInt,
                                         std::vector<double> quadratureWeights);

  //! Returns the dot product of two PetscScalar arrays
  /*! \note the two arrays much have the same length

      \param v1
        Array of length l

      \param v2
        Array of length l

      \param l
        Length of arrays v1 and v2

      \return
        Dot product of the two arrays
  */
  PetscScalar Dot (PetscScalar *v1, PetscScalar *v2, PetscInt l);

  //! Computes differentiated shape functions at the point given by (xi, eta,
  //! zeta)
  /*! \param xi
        Isoparametric coordinate

      \param eta
        Isoparametric coordinate

      \param zeta
        Isoparametric coordinate

      \param dNdxi
        Derivative of shape function with respect to xi

      \param dNdeta
        Derivative of shape function with respect to eta

      \param dNdzeta
        Derivative of shape function with respect to zeta
  */
  void DifferentiatedShapeFunctions (PetscScalar xi, PetscScalar eta, PetscScalar zeta,
                                     PetscScalar *dNdxi, PetscScalar *dNdeta, PetscScalar *dNdzeta);

  //! Calculates the inverse of a 3x3 matrix and returns its determinant.
  /*! \param J
        3x3 matrix.

      \param invJ
        Inverse of the 3x3 matrix J.

      \return
        Determinant of the matrix J.
  */
  PetscScalar Inverse3M (PetscScalar J[][3], PetscScalar invJ[][3]);
  ///\}

  /*! \name Added by Sandy*/
  ///\{
  PetscScalar BMatrix[6][24];                 //!< B-Matrix for stress computations
  std::vector<std::vector<double>> CPrivate;  //!< Constitutive matrix
  bool isBMatrixVectorComputed = false;
  bool isVCBVectorComputed = false;

  //! B-Matrix for stress computations
  std::vector<std::vector<std::vector<double>>> BMatrixVector;

  //! VCB for stress computations
  std::vector<std::vector<std::vector<double>>> VCBVector;
  ///\}

  /*! \name Thermal stuff added by Sandy*/
  ///\{
  std::vector<double> fThhermalElem;  //!< elemental thermal force
  Vec RHSthermal;                     //!< Assembled thermal force
                                      ///\}
};

}  // namespace para_fea

#endif