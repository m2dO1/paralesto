//
// Copyright 2022 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "initialize_lsm.h"

#include <memory>

namespace para_lsm {

InitializeLsm::InitializeLsm () {
  // Mesh size
  nelx = 128;
  nely = 64;
  nelz = 64;

  // Fix a cuboid (square region at the base of design domain)
  static para_lsm::Cuboid cube0 (nelx / 2.0, nely / 2.0, nelz - 2.0, nelx / 2.0, nely / 2.0, 2.0);
  fixedBlobs.push_back (std::make_unique<para_lsm::Cuboid> (cube0));

  // // Use discrete adjoint sensitivity calculation
  // map_flag = 1 ;

  // // Set perturbation size for sensitivity calculation
  // perturbation = 0.15 ;
}

}  // namespace para_lsm