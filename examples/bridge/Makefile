PETSC_DIR=~/petsc
PETSC_ARCH=arch-linux-c-debug
COMP = mpic++
CFLAGS = -I.
FFLAGS=
CPPFLAGS= -fopenmp -std=c++14 -O3 -w
FPPFLAGS=
DFLAGS= -D SAVEMEMORY
LOCDIR=
EXAMPLESC=
EXAMPLESF=
MANSEC=
CLEANFILES=
NP=
EIGEN_DIR = ~/eigen3
LSM_DIR = ./../../lsm
FEA_DIR = ./../../fea
IO_DIR = ./../../wrapper_io
INCLUDEOTHER= -I. -I $(LSM_DIR)/include -I $(FEA_DIR)/include \
-I $(IO_DIR)/include -I ${PETSC_DIR}/${PETSC_ARCH}/include -I $(EIGEN_DIR) 
LIBOTHER= -lnlopt -lglpk
LFLAGOTHER= 

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules
include ${PETSC_DIR}/lib/petsc/conf/test


# Directory where you want your .o files stored
BUILDDIR = ../bin

# List of object .o files
TARGETS = $(BUILDDIR)/levelset.o \
$(BUILDDIR)/optimize.o \
$(BUILDDIR)/grid_math.o \
$(BUILDDIR)/boundary.o \
$(BUILDDIR)/topopt.o \
$(BUILDDIR)/linearelasticity.o \
$(BUILDDIR)/topopt1d.o \
$(BUILDDIR)/poisson.o \
$(BUILDDIR)/wrapper.o \
$(BUILDDIR)/initialize.o \
$(BUILDDIR)/levelsetwrap.o \
$(BUILDDIR)/optimizerwrap.o \
$(BUILDDIR)/initialize_lsm.o \
$(BUILDDIR)/physics_wrapper.o \
$(BUILDDIR)/wrapper_io.o \
$(BUILDDIR)/utils.o

topopt: $(TARGETS) main.o
	rm -rf topopt.out
	-${CLINKER} -O3 -std=c++14 ${INCLUDEOTHER} -o topopt.out $(TARGETS) \
	main.o ${PETSC_SYS_LIB} ${LIBOTHER} ${RPATH}

main.o: main.cpp
	-${CLINKER} ${CPPFLAGS} -c -o main.o main.cpp -I ${PETSC_DIR}/include \
	${INCLUDEOTHER}


.PHONY: all myclean clean_build
all: $(TARGETS)

# Make commands for each individual object file
$(BUILDDIR)/topopt.o: $(FEA_DIR)/src/topopt.cpp $(FEA_DIR)/include/topopt.h | $(BUILDDIR)
	-${CLINKER} ${DFLAGS} ${CPPFLAGS} -c -o $(BUILDDIR)/topopt.o \
	$(FEA_DIR)/src/topopt.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(BUILDDIR)/topopt1d.o: $(FEA_DIR)/src/topopt1d.cpp $(FEA_DIR)/include/topopt1d.h | $(BUILDDIR)
	-${CLINKER} ${DFLAGS} ${CPPFLAGS} -c -o $(BUILDDIR)/topopt1d.o \
	$(FEA_DIR)/src/topopt1d.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(BUILDDIR)/linearelasticity.o: $(FEA_DIR)/src/linear_elasticity.cpp \
$(FEA_DIR)/include/linear_elasticity.h | $(BUILDDIR)
	-${CLINKER} ${DFLAGS} ${CPPFLAGS} -c -o $(BUILDDIR)/linearelasticity.o \
	$(FEA_DIR)/src/linear_elasticity.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(BUILDDIR)/poisson.o: $(FEA_DIR)/src/poisson.cpp $(FEA_DIR)/include/poisson.h | $(BUILDDIR)
	-${CLINKER} ${DFLAGS} ${CPPFLAGS} -c -o $(BUILDDIR)/poisson.o \
	$(FEA_DIR)/src/poisson.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(BUILDDIR)/wrapper.o: $(FEA_DIR)/src/wrapper.cpp $(FEA_DIR)/include/wrapper.h | $(BUILDDIR)
	-${CLINKER} ${CPPFLAGS} -c -o $(BUILDDIR)/wrapper.o $(FEA_DIR)/src/wrapper.cpp \
	-I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(BUILDDIR)/physics_wrapper.o: $(FEA_DIR)/src/physics_wrapper.cpp \
$(FEA_DIR)/include/physics_wrapper.h | $(BUILDDIR)
	-${CLINKER} ${CPPFLAGS} -c -o $(BUILDDIR)/physics_wrapper.o \
	$(FEA_DIR)/src/physics_wrapper.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(BUILDDIR)/levelset.o: $(LSM_DIR)/src/lsm_3d.cpp $(LSM_DIR)/include/lsm_3d.h | $(BUILDDIR)
	$(COMP) $(CPPFLAGS) -c -o $(BUILDDIR)/levelset.o $(LSM_DIR)/src/lsm_3d.cpp \
	${INCLUDEOTHER}

$(BUILDDIR)/levelsetwrap.o: $(LSM_DIR)/src/level_set_wrapper.cpp \
$(LSM_DIR)/include/level_set_wrapper.h | $(BUILDDIR)
	$(COMP) $(CPPFLAGS) -c -o $(BUILDDIR)/levelsetwrap.o \
	$(LSM_DIR)/src/level_set_wrapper.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(BUILDDIR)/optimizerwrap.o: $(LSM_DIR)/src/optimizer_wrapper.cpp \
$(LSM_DIR)/include/optimizer_wrapper.h | $(BUILDDIR)
	$(COMP) $(CPPFLAGS) -c -o $(BUILDDIR)/optimizerwrap.o \
	$(LSM_DIR)/src/optimizer_wrapper.cpp -I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(BUILDDIR)/optimize.o: $(LSM_DIR)/src/optimize.cpp $(LSM_DIR)/include/optimize.h | $(BUILDDIR)
	$(COMP) $(CPPFLAGS) -c -o $(BUILDDIR)/optimize.o $(LSM_DIR)/src/optimize.cpp \
	${INCLUDEOTHER}

$(BUILDDIR)/boundary.o: $(LSM_DIR)/src/boundary.cpp $(LSM_DIR)/include/boundary.h | $(BUILDDIR)
	$(COMP) $(CPPFLAGS) -c -o $(BUILDDIR)/boundary.o $(LSM_DIR)/src/boundary.cpp \
	${INCLUDEOTHER}

$(BUILDDIR)/grid_math.o: $(LSM_DIR)/src/grid_math.cpp $(LSM_DIR)/include/grid_math.h | $(BUILDDIR)
	$(COMP) $(CPPFLAGS) -c -o $(BUILDDIR)/grid_math.o $(LSM_DIR)/src/grid_math.cpp \
	${INCLUDEOTHER}

$(BUILDDIR)/wrapper_io.o: $(IO_DIR)/src/wrapper_io.cpp \
$(IO_DIR)/include/wrapper_io.h | $(BUILDDIR)
	$(COMP) $(CPPFLAGS) -c -o $(BUILDDIR)/wrapper_io.o $(IO_DIR)/src/wrapper_io.cpp \
	-I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(BUILDDIR)/utils.o: $(IO_DIR)/src/utils.cpp $(IO_DIR)/include/utils.h | $(BUILDDIR)
	$(COMP) $(CPPFLAGS) -c -o $(BUILDDIR)/utils.o $(IO_DIR)/src/utils.cpp \
	-I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(BUILDDIR)/initialize.o: initialize.cpp $(FEA_DIR)/include/initialize.h | $(BUILDDIR)
	-$(COMP) $(CPPFLAGS) -c -o $(BUILDDIR)/initialize.o initialize.cpp \
	-I ${PETSC_DIR}/include ${INCLUDEOTHER}

$(BUILDDIR)/initialize_lsm.o: initialize_lsm.cpp $(LSM_DIR)/include/initialize_lsm.h | $(BUILDDIR)
	$(COMP) $(CPPFLAGS) -c -o $(BUILDDIR)/initialize_lsm.o initialize_lsm.cpp \
	${INCLUDEOTHER}

myclean:
	rm -rf *.o *.out

clean_build:
	rm -f $(OBJ)

$(BUILDDIR):
	mkdir $(BUILDDIR)