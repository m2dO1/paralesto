//
// Copyright 2022 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <mpi.h>
#include <omp.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "paralesto.h"
#include "petsc.h"

namespace fea = para_fea;
namespace lsm = para_lsm;

static char help[] = "3D TopOpt using KSP-MG on PETSc's DMDA (structured grids) \n";

int main (int argc, char* argv[]) {
  //============================================================================
  // Set up physics module
  //----------------------------------------------------------------------------
  // Error code for debugging
  PetscErrorCode ierr;

  // Initialize PETSc / MPI and pass input arguments to PETSc
  PetscInitialize (&argc, &argv, PETSC_NULL, help);

  // Declare and define rank variable
  // All level set operations should occur on a single processor (rank 0)
  PetscMPIInt rank = 0;
  MPI_Comm_rank (PETSC_COMM_WORLD, &rank);
  // MPI_Comm_size(PETSC_COMM_WORLD, &size); // ???? think this is taken care of in physics_wrapper?
  // MPI_Barrier?

  // Initialize the input file
  fea::InitializeOpt phys_init;

  // Create physics (FEA) object
  fea::PhysicsWrapper physics_solver (phys_init);
  fea::TopOpt1D* optHeat = new fea::TopOpt1D (phys_init);                       // ????
  fea::Poisson* physicsHeat = new fea::Poisson (optHeat->da_nodes, phys_init);  // ????
  if (rank == 0) std::cout << "physics module setup finished" << std::endl;


  //============================================================================
  // Set up level set module
  //----------------------------------------------------------------------------
  // Initialize the input file object
  lsm::InitializeLsm lsm_init;

  // Create level set object
  lsm::LevelSetWrapper lsm;
  if (rank == 0) {
    lsm = lsm::LevelSetWrapper (lsm_init);
    std::cout << "level set module setup finished" << std::endl;
  }


  //============================================================================
  // Set up optimizer module
  //----------------------------------------------------------------------------
  // Define parameters
  int num_cons = 1;  // single volume constraint
  double total_volume = lsm_init.nelx * lsm_init.nely * lsm_init.nelz;
  std::vector<double> max_cons_vals = {0.1 * total_volume};  // max volume
  int opt_algo = 0;                                          // optimization algorithm, simplex

  // Create optimizer object
  lsm::OptimizerWrapper opt (num_cons, max_cons_vals, opt_algo);

  if (rank == 0) std::cout << "optimizer module setup finished" << std::endl;


  //============================================================================
  // Define necessary parameters and data structures for optimization loop
  //----------------------------------------------------------------------------
  int max_iter = 200;        // Maximum # of iterations
  int count_iter = 0;        // counter for current iteration #
  double move_limit = 0.25;  // max amount the level set can move per iteration
  bool is_print = 1;         // print progress statements during method runtime
  std::vector<double> densities, df_drho, df_bpt, velocities;

  // Vector for sensitivities of each constraint
  std::vector<std::vector<double> > cons_sens (num_cons);

  // Vector for current value of each constraint
  std::vector<double> curr_cons_vals (num_cons);

  // Vector of upper and lower limits for boundary point movement
  std::vector<double> upper_lim, lower_lim;

  // Object for printing/writing results
  paralesto::WrapperIO io (lsm, physics_solver);


  //============================================================================
  // Main body
  //----------------------------------------------------------------------------
  if (rank == 0) std::cout << "\nStarting main...\n" << std::endl;

  // Optimization loop
  double iter_start_time, iter_end_time;
  while (count_iter < max_iter) {
    iter_start_time = MPI_Wtime ();
    // Update iteration counter
    count_iter++;

    if (rank == 0) {
      // Get the densities of the level set mesh
      densities = lsm.CalculateElementDensities (is_print);

      // Get ordering for the PETSc mesh
      densities = paralesto::LsmToPetscDensity (densities, physics_solver.GetLsmPetscMap ());
    }

    // Calculate the partial derivative with respect to element densities
    df_drho = physics_solver.CalculatePartialThermalCompliance (densities, is_print);

    // Level set and optimizer operations
    if (rank == 0) {
      // Get ordering for the level set grid
      df_drho = paralesto::PetscToLsmSensitivity (df_drho, physics_solver.GetLsmPetscMap ());

      // Map element partials to boundary points
      df_bpt = lsm.MapSensitivities (df_drho, is_print);
      std::vector<double> dg_bpt = lsm.MapVolumeSensitivities ();
      cons_sens[0] = dg_bpt;

      // Solve the suboptimization problem
      curr_cons_vals[0] = lsm.GetVolume ();              // get current volume
      lsm.GetLimits (upper_lim, lower_lim, move_limit);  // get limits for each bpt movement
      opt.SetLimits (upper_lim, lower_lim);              // set limits for the optimizer
      velocities = opt.Solve (df_bpt, cons_sens, curr_cons_vals, is_print);

      // Update the topology by advecting the level set
      lsm.Update (velocities, move_limit, is_print);
    }

    // Print output
    iter_end_time = MPI_Wtime ();
    if (rank == 0) {
      double iter_time = iter_end_time - iter_start_time;
      std::cout << "Completed update. Iteration time = " << iter_time << std::endl;
      io.PrintPoissonIter (count_iter);
      int stl_iter = count_iter - 1;
      if (stl_iter % 5 == 0) io.WriteStl (stl_iter);  // output every 5 iterations
    }

    if (rank == 0) std::cout << "" << std::endl;
  }

  PetscFinalize ();
  if (rank == 0) std::cout << "...finished main. :D\n" << std::endl;
}