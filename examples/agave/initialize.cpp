//
// Copyright 2022 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "initialize.h"

namespace para_fea {

InitializeOpt::InitializeOpt () {
  // Mesh size
  nelx = 160;
  nely = 160;
  nelz = 160;

  // Physical dimensions of design domain
  lx = 1.0;  // [m]
  ly = 1.0;  // [m]
  lz = 1.0;  // [m]

  // Number of multigrid levels
  nlvls = 3;

  // Material properties
  Emax = 113.8e9;        // Elastic moduli of material
  Emin = 1.0e-9 * Emax;  // Elastic moduli of void
  nu = 0.342;            // Poisson's ratio
  Kmax = 6.7;            // Conductivity coefficient of solid material [W/mK]
  Kmin = 6.7 * 0.005;    // Conductivity coefficient of void material [W/mK]

  // Mark that the physics is a poisson (heat conduction) problem
  is_poisson_phys = true;


  // some constants
  double dx = lx / nelx;
  double dy = ly / nely;
  double dz = lz / nelz;
  double eps = 0.1 * std::min ({dx, dy, dz});  // small value
  double infi = 10.0 * std::max ({lx, ly, lz});

  // Apply temperature boundary condition
  HeatLoad fixed_temp
      = HeatLoad (0.0,                           // temperature value
                  lx / 2.0, ly / 2.0, 0.0 * dz,  // x,y,z location of the centroid
                  lx / 20.0 + eps, ly / 20.0 + eps, 1 * dz + eps);  // tolerance on the nodes
  fixedHeatDof.push_back (fixed_temp);

  // Apply heat load everywhere except at the base
  HeatLoad heat_load = HeatLoad (1.0e-6,                            // heat load [W]
                                 0 * dx, 0 * dy, lz,                // x,y,z location of the node
                                 infi, infi, lz - (2 * dz + eps));  // tolerance on the nodes
  heatLoad.push_back (heat_load);
}


Force::Force () : valx (0), valy (0), valz (0), x (0), y (0), z (0), tolx (0), toly (0), tolz (0) {}


Force::Force (double valx_, double valy_, double valz_, double x_, double y_, double z_,
              double tolx_, double toly_, double tolz_)
    : valx (valx_),
      valy (valy_),
      valz (valz_),
      x (x_),
      y (y_),
      z (z_),
      tolx (tolx_),
      toly (toly_),
      tolz (tolz_) {}


FixedDof::FixedDof (bool valx_, bool valy_, bool valz_, double x_, double y_, double z_,
                    double tolx_, double toly_, double tolz_)
    : valx (valx_),
      valy (valy_),
      valz (valz_),
      x (x_),
      y (y_),
      z (z_),
      tolx (tolx_),
      toly (toly_),
      tolz (tolz_) {}


HeatLoad::HeatLoad () : val (0), x (0), y (0), z (0), tolx (0), toly (0), tolz (0) {}


HeatLoad::HeatLoad (double val_, double x_, double y_, double z_, double tolx_, double toly_,
                    double tolz_)
    : val (val_), x (x_), y (y_), z (z_), tolx (tolx_), toly (toly_), tolz (tolz_) {}

}  // namespace para_fea