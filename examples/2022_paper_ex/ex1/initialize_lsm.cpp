//
// Copyright 2022 H Alicia Kim
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "initialize_lsm.h"

#include <memory>

namespace para_lsm {

InitializeLsm::InitializeLsm () {
  // Mesh size
  nelx = 40;
  nely = 40;
  nelz = 40;

  // Create initial holes (cube shape) in design domain
  for (int i = 1; i < 4; i++) {
    for (int j = 1; j < 4; j++) {
      double xc = i * 10;
      double yc = j * 10;
      Cuboid hole (xc, yc, nelz,  // Cubeoid center
                   2, 2, nelz);   // half widths of the cuboid
      initialVoids.push_back (std::make_shared<Cuboid> (hole));
    }
  }

  // Fix a cubeoid (square region at the base of design domain)
  Cuboid base (0.0, 0.5 * nely, 0.5 * nelz,            // Cubeoid center
               0.1 * nelx, 0.05 * nely, 0.05 * nelz);  // half widths of the cubeoid
  fixedBlobs.push_back (std::make_shared<Cuboid> (base));

  // Use discrete adjoint sensitivity calculation
  map_flag = 1;

  // Set perturbation size for sensitivity calculation
  perturbation = 0.15;
}

}  // namespace para_lsm