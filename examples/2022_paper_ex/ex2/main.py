#
# Copyright 2022 H Alicia Kim
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from fenics import *
from fenics_adjoint import *
import numpy as np
from ufl import nabla_div

from pyparalesto.pylsm import PyInput, PyLevelSetModule
from pyparalesto.pyopt import PyOptimizerModule
from pyparalesto.py_fea.solver import PoissonPde

if __name__ == "__main__":
    # Print that test has started
    print("Main script start")

    ############################################################################
    # Set up PDE for linear elasticity
    ############################################################################
    # Define parameters
    nelx    = 40
    nely    = 40
    nelz    = 40
    lx      = 1.0 # [m]
    ly      = 1.0 # [m]
    lz      = 1.0 # [m]
    kappa   = 1.0 # [W/mK]
    rho_min = 1e-5
    dx = lx/float(nelx)
    dy = ly/float(nely)
    dz = lz/float(nelz)

    # Create linear elasticity solver
    pde = PoissonPde(nelx, nely, nelz, lx, ly, lz, kappa, rho_min)

    # Add boundary condition
    pde.add_temperature_bc(0.0, 0.5*ly, 0.5*lz, # x, y, z coordinate of center
        dx, 0.05*ly, 0.05*lz, # half-width in each direction
        0.0) # prescribed temperature

    # Apply the body load
    pde.apply_body_load(0.0, 0.0, 0.0, # x, y, z coordinate of center
        lx, ly, lz, # half-width in each direction
        1.0) # heat load [W]

    # Define the linear form L(v) of the variational problem
    pde.define_linear_form()


    ############################################################################
    # Set up level set module
    ############################################################################
    # Initialize input object
    map_flag = 1 # use discrete adjoint
    perturbation = 0.15
    init = PyInput(nelx, nely, nelz, map_flag, perturbation)

    # Add a void initial region
    for i in range(1, 4):
        for j in range(1, 4):
            init.add_initial_void_cuboid(10*i, 10*j, nelz, # cubeoid center
                2, 2, nelz) # half widths of cuboid

    # Fix a cuboid (where temperature bc condition is applied)
    init.add_nondesign_domain_cuboid(0.0, 0.5*nely, 0.5*nelz, # cubeoid center
        0.1*nelx, 0.05*nely, 0.05*nelz) # half widths of cubeoid

    # Create a level set module from input object
    lsm = PyLevelSetModule(init)


    ############################################################################
    # Set up optimization module
    ############################################################################
    # Create an optimization module
    num_cons      = 1
    total_volume  = nelx*nely*nelz
    max_cons_vals = np.array([0.3*total_volume])
    opt_algo      = 1 # optimization algorithm, nlopt
    opt = PyOptimizerModule(num_cons, max_cons_vals, opt_algo)


    ############################################################################
    # Define necessary parameters and data structures for optimization loop
    ############################################################################
    # Definite parameters
    curr_cons_vals = np.zeros(num_cons, dtype=np.double)
    max_iter       = 401
    count_iter     = 0
    move_limit     = 0.4
    is_print       = True


    ############################################################################
    # Optimization loop
    ############################################################################
    while count_iter < max_iter:
        # Get the element densities
        densities = lsm.calculate_element_densities(is_print)

        # Solve the pde
        pde.define_bilinear_form(densities) # a(u,v) of the variational problem
        pde.solve() # Solve pde
        print("Finished pde solve")

        # Calculate the partial derivative wrt element densities
        J = assemble(action(pde.L, pde.u_sol)) # define compliance
        control = Control(pde.density) # define density as the design variable
        dJ_drho = compute_gradient(J, control)
        print("Finished sensitivity calculation")

        # Map element partials to boundary points
        df_bpt = lsm.map_sensitivities(dJ_drho.vector()[:], is_print)
        dg_bpt = lsm.map_volume_sensitivities()

        # Solve the suboptimization problem
        curr_cons_vals[0] = lsm.get_volume()
        if count_iter > 50:
            move_limit = 0.05
        limits = lsm.get_limits(move_limit)
        opt.set_limits(limits)
        velocities = opt.solve(df_bpt, dg_bpt, curr_cons_vals, is_print)

        # Update the topology advecting the level set
        lsm.update(velocities, move_limit, is_print)

        # Write stl of level set boundary every 10 iterations
        if count_iter % 10 == 0:
            lsm.write_stl(count_iter)

        count_iter += 1
        vol_cons = curr_cons_vals[0] / float(nelx*nely*nelz)
        compliance = float(J)
        print('Iter = ' + str(count_iter) + '; Vol cons = ' + str(vol_cons) +
            '; Objective = ' + str(compliance) + '\n')

    # Print that test has completed
    print("Main script end")